<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html lang="en" dir="ltr"
      prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile# video: http://ogp.me/ns/video# product: http://ogp.me/ns/product# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
   
    <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" href="http://localhost/images/logo1.jpeg"
          type="image/vnd.microsoft.icon"/>
    <meta name="description" content="Katihar Engineering College, Katihar"/>
    <meta name="robots" content="follow, index"/
    <link rel="canonical" href="http://www.localhost"/>
    <link rel="shortlink" href="http://www.localhost"/>
    <meta property="og:site_name" content="KEC Katihar"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="http://www.localhost"/>
    <meta property="og:title" content="KEC Katihar"/>
    <meta itemprop="name" content="About Us"/>
    <meta name="dcterms.title" content="KEC Katihar"/>
    <meta name="dcterms.type" content="Text"/>
    <meta name="dcterms.format" content="text/html"/>
    <meta name="dcterms.identifier" content="http://localhost"/>
    <link rel="stylesheet" href="http://localhost/css/normalize.css">
        <link rel="stylesheet" href="http://localhost/css/main.css">
        <script src="http://localhost/js/vendor/modernizr-2.6.2.min.js"></script>
    
    <title>KATIHAR ENGINEERING COLLEGE, KATIHAR</title>
                   
</head>

<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-page">

<header id="navbar" role="banner" class=" navbar navbar-default">
    <div class="container">

        <div class="gt-topheader">
            <div class="row">
                <div class="col-sm-4 loading">
                    <a class="logo navbar-btn pull-left loading" href="/" title="Home" >
                        <img src="http://localhost/images/logo.png" alt="Home"/>
                    </a>
                </div>
                <div class="col-sm-8">
                    <div class="region region-navigation">
                        <section id="block-block-8" class="block block-block clearfix">
            



                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="http://localhost/academic/antiragging">Anti Ragging</a></li>
                                <li><a href="http://localhost/rti/rtiact">RTI</a></li>
                                <li><a href="http://localhost/approval/aicteapproval" target="_blank">AICTE</a></li>
                                <li><a href="http://localhost/category/kecwebteam"target="_blank">KEC Web Team</a></li>
                            </ul>

                        </section>
                        <section id="block-search-form" class="block block-search clearfix">
                                    <div>
                                        <h2 class="element-invisible">Search form</h2>
                                        <ul>
                                        <li>
                                        <form id = "searchFacultyName">
                                        <div class="col-md-7 text-center  "><input type="text"placeholder="Search Faculty" name="facultyName" id = "facultyNames" /></div>  
                                        </li>
                                        <button class="glyphicon glyphicon-search"id="search"style="font-size:170%"></button>
                                      </br>
                                           </form>
                                            

                                        </ul>
                                </div>

                        </section>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-collapse">
            <nav role="navigation">
            
            
                <div class="region region-rwsmainmenu">
                    <section id="block-superfish-1" class="block block-superfish clearfix">
                        <ul id="superfish-1" class="nav-links menu sf-menu sf-main-menu sf-horizontal sf-style-none sf-total-items-10 sf-parent-items-9 sf-single-items-1">
                            <li id="menu-218-1" class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/" class="sf-depth-1 ">Home</a></li>
                            <li id="menu-1131-1" class="middle odd sf-item-5 sf-depth-1 sf-total-children-7 sf-parent-children-0 sf-single-children-7 menuparent">
                                <a href="http://localhost/about/history" class="sf-depth-1 menuparent">About KEC</a>
                                <ul>
                                    <li id="menu-1321-12" cl ass="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/about/history" class="sf-depth-2">History</a></li>
                                    <li id="menu-1462-13" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="http://localhost/about/administration" class="sf-depth-2">Administration</a></li>
                                    <li id="menu-1282-14" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="http://localhost/about/principal" class="sf-depth-2">Principal's Message</a></li>
                                    <li id="menu-1132-15" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="http://localhost/about/vision" title="" class="sf-depth-2">Vision &amp; Mission</a></li>
                                    <li id="menu-1133-16" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="http://localhost/about/affiliation" class="sf-depth-2">Affiliation</a></li>
                                    <li id="menu-1321-17" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/about/allotment" class="sf-depth-2">Allotment &amp; Surrender Report</a></li>
                                </ul>
                            </li>
            
                            <li id="menu-1131-1" class="middle odd sf-item-5 sf-depth-1 sf-total-children-7 sf-parent-children-0 sf-single-children-7 menuparent">
                                <a href="http://localhost/academic/admission" class="sf-depth-1 menuparent">Academic</a>
                                <ul>
                                    <li id="menu-1321-12" cl ass="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/academic/admission" class="sf-depth-2">Admission</a></li>
                                    <li id="menu-1462-13" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="http://localhost/academic/regulations" class="sf-depth-2">Academic Regulations</a></li>
                                    <li id="menu-1282-14" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="http://localhost/academic/calender" class="sf-depth-2">Academic Calender</a></li>
                                    <li id="menu-1132-15" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="http://localhost/academic/holidays" title="" class="sf-depth-2">List of Holidays</a></li>
                                    <li id="menu-1133-16" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="http://localhost/academic/attendance" class="sf-depth-2">Attendance</a></li>
                                    <li id="menu-1321-17" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/academic/syllabus" class="sf-depth-2">Syllabus</a></li>
                                    <li id="menu-1311-18" class="middle even sf-item-6 sf-depth-2 sf-no-children"><a href="http://localhost/academic/rules" class="sf-depth-2">Disciplinary Rules</a></li>
                                    <li id="menu-1311-18" class="middle even sf-item-6 sf-depth-2 sf-no-children"><a href="http://localhost/academic/paymenttutorial" class="sf-depth-2"> Online Fee Payment Tutorial</a></li>
                                    <li id="menu-1311-18" class="middle even sf-item-6 sf-depth-2 sf-no-children"><a href="http://localhost/academic/antiragging" class="sf-depth-2"> Anti Ragging</a></li>
                                    <li id="menu-1321-20" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/academic/academiccouncil" class="sf-depth-2">MoM of Academic Council</a></li>
                                    <li id="menu-1321-21" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/academic/notice" class="sf-depth-2">Notice from Govt.</a></li>
                                    | </ul>
                            </li>
            
                            <li id="menu-1131-1" class="middle odd sf-item-5 sf-depth-1 sf-total-children-7 sf-parent-children-0 sf-single-children-7 menuparent">
                                <a href="http://localhost/category/department" class="sf-depth-1 menuparent">Departments</a>
                                <ul>
                                    <li id="menu-1321-12" cl ass="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/department/civil/about" class="sf-depth-2">Civil Engineering</a></li>
                                    <li id="menu-1462-13" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="http://localhost/department/mechanical/mech" class="sf-depth-2">Mechanical Engineering</a></li>
                                    <li id="menu-1282-14" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="http://localhost/department/cse/about" class="sf-depth-2">Computer Sc. & Engineering</a></li>
                                    <li id="menu-1132-15" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="http://localhost/department/eee/about" title="" class="sf-depth-2">Electrical & Electronics Engg.</a></li>
                                    <li id="menu-1133-16" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="http://localhost/department/ash/about" class="sf-depth-2">Applied Sc. & Humanities</a></li>
                                </ul>
                            </li>
                            <li id="menu-1131-1" class="middle odd sf-item-5 sf-depth-1 sf-total-children-7 sf-parent-children-0 sf-single-children-7 menuparent">
                                <a href="http://localhost/facilities/csecenter" class="sf-depth-1 menuparent">Facilities</a>
                                <ul>
                                    <li id="menu-1321-12" cl ass="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/bank" class="sf-depth-2">Bank</a></li>
                                    <li id="menu-1462-13" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/centrallibrary" class="sf-depth-2">Central Library</a></li>
                                    <li id="menu-1282-14" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/csecenter" class="sf-depth-2">Computer Centre</a></li>
                                    <li id="menu-1132-15" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/club" title="" class="sf-depth-2">Club</a></li>
                                    <li id="menu-1133-16" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/guesthouse" class="sf-depth-2">Guest House</a></li>
                                    <li id="menu-1321-17" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/gymnasium" class="sf-depth-2">Gymnasium</a></li>
                                    <li id="menu-1311-18" class="middle even sf-item-6 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/healthcenter" class="sf-depth-2">Health Centre</a></li>
                                    <li id="menu-1401-19" class="last odd sf-item-7 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/hostels" title="Hostels" class="sf-depth-2">Hostels</a></li>
                                    <li id="menu-1321-20" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/medicalfacilities" class="sf-depth-2">Medical Facilities</a></li>
                                    <li id="menu-1321-21" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/placementbrouchure" class="sf-depth-2">Placement</a></li>
                                    <li id="menu-1321-22" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/sportsfacilities" class="sf-depth-2">Sports Facilities</a></li>
                                    <li id="menu-1321-23" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/startupcell" class="sf-depth-2">Startup cell</a></li>
                                    <li id="menu-1321-24" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/usefulllink" class="sf-depth-2">Useful Link</a></li>
                                    <li id="menu-1321-25" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/facilities/wifi" class="sf-depth-2">Wi Fi</a></li>
                                </ul>
                            </li>
                            <li id="menu-1131-1" class="middle odd sf-item-5 sf-depth-1 sf-total-children-7 sf-parent-children-0 sf-single-children-7 menuparent">
                                <a href="http://localhost/trainingplacement/placementbrochure" class="sf-depth-1 menuparent">Training & Placement</a>
                                <ul>
                                    <li id="menu-1321-12" cl ass="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/trainingplacement/placementbrochure" class="sf-depth-2">About Placement</a></li>
                                    <li id="menu-1462-13" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="http://localhost//trainingplacement/placementbrochure" class="sf-depth-2">Placement Brouchure</a></li>
                                    <li id="menu-1282-14" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="http://localhost//trainingplacement/rulesoftrainingplacement" class="sf-depth-2">Rules of training & Placement</a></li>
                                    <li id="menu-1132-15" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="http://localhost/trainingplacement/tipsforresume" title="" class="sf-depth-2">Tips for Resume</a></li>
                                    <li id="menu-1133-16" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="http://localhost/trainingplacement/studentplacementcoordinator" class="sf-depth-2">Student Placement Coordinator</a></li>
                                </ul>
                            </li>
                            <li id="menu-1131-1" class="middle odd sf-item-5 sf-depth-1 sf-total-children-7 sf-parent-children-0 sf-single-children-7 menuparent">
                                <a href="http://localhost/approval/akuapproval" class="sf-depth-1 menuparent">Approval</a>
                                <ul>
                                    <li id="menu-1321-12" cl ass="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/approval/aicteapproval" class="sf-depth-2">AICTE Aproval</a></li>
                                    <li id="menu-1462-13" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="http://localhost/approval/akuapproval" class="sf-depth-2">AKU Approval</a></li>
                                </ul>
                            </li>
                            <li id="menu-1131-1" class="middle odd sf-item-5 sf-depth-1 sf-total-children-7 sf-parent-children-0 sf-single-children-7 menuparent">
                                <a href="http://localhost/rti/rtiact" class="sf-depth-1 menuparent">RTI</a>
                                <ul>
                                    <li id="menu-1321-12" cl ass="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/rti/rtiact" class="sf-depth-2">RTI Act 2005</a></li>
                                    <li id="menu-1462-13" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="http://localhost/rti/rtiapplication" class="sf-depth-2">RTI Application Form</a></li>
                                    <li id="menu-1282-14" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="http://localhost//rti/rtistatus" class="sf-depth-2">RTI Status</a></li>
                                    <li id="menu-1132-15" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="http://localhost/rti/publicinformationcell" title="" class="sf-depth-2">Public Information Cell</a></li>
                                </ul>
                            </li>
                            <li id="menu-1131-1" class="middle odd sf-item-5 sf-depth-1 sf-total-children-7 sf-parent-children-0 sf-single-children-7 menuparent">
                                <a href="http://localhost/gallery/mediagallery" class="sf-depth-1 menuparent">Gallery</a>
                                <ul>
                                    <li id="menu-1321-12" cl ass="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://localhost/gallery/mediagallery" class="sf-depth-2">Media Gallery</a></li>
            
                                </ul>
                            </li>
            
                            <sec:authorize access="!isAuthenticated()">
                               <li >
                                <a href="http://localhost/login" title="" class="sf-depth-1 menuparent">Login</a>
                               </li>
                             </sec:authorize>
                             <sec:authorize access="isAuthenticated()">
		                            <li>
                                <a href="http://localhost/logout" title="" class="sf-depth-1 menuparent">Logout</a>
                              </li>
                                   </sec:authorize>
                        </ul>
                    </section>
                </div>
            </nav>
        </div>
    </div>
</header>
<link rel="stylesheet" href="http://localhost/css/jquery-ui.css">
<script src="http://localhost/js/jquery.min.js"></script>
<script src="http://localhost/js/jquery-ui.js"></script>
<script src="http://localhost/js/searchFaculty.js"></script>
<style>
.ui-autocomplete{
z-index:100000000
}

@media(min-width:768px)
.region{
        display:block
}
</style>
