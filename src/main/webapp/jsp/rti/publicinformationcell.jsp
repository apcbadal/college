<jsp:include page="/jsp/header.jsp"/>


<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-8"><h3>Public Information Cell</h3></div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
           <aside class="col-sm-3" role="complementary">
                <div class="region region-sidebar-second">
                  <section id="block-menu-block-3" class="block block-menu-block clearfix">
                     <h2 class="block-title">Right To Information</h2>
                    <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                        <ul class="menu nav">
                            <li class="first leaf  menu-mlid-1154"><a href="http://localhost/rti/rtiapplication" class="sf-depth-2 active">RTI Application Form</a></li>
                              <li class="leaf menu-mlid-1121"><a href="http://localhost/rti/rtistatus" class="sf-depth-2">RTI Status</a></li>
                              <li class="leaf menu-mlid-1121"><a href="http://localhost/rti/publicinformationcell" class="sf-depth-2">Public Information Cell</a></li>
                               <li class="leaf menu-mlid-1121"><a href="http://localhost/rti/rtiact" class="sf-depth-2">RTI Act</a></li>
                         </ul>
                    </div>
                  </section>
                  </div>
               </aside>

        <div class="col-md-8">
            <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                <div class="WordSection1">


                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p class="MsoNormal"></div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="/jsp/footer.jsp"/>
