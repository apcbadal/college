<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>UG Admission</h3></div>
            </div>
        </div>
    </div>
    <div class="container">



        <div class="row">
            <aside class="col-sm-3" role="complementary">
                                                                            <div class="region region-sidebar-second">
                                                                  <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                                          <h2 class="block-title">Academics</h2>

                                                                    <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                                                   <ul class="menu nav">
                                                                  <li class="first leaf  menu-mlid-1154"><a href="http://localhost/academic/admission" class="sf-depth-2 active">UG Admission</a></li>
                                                                  <li class="leaf menu-mlid-1121"><a href="http://localhost/academic/regulations" class="sf-depth-2">Academic Regulations</a></li>
                                                                  <li class="leaf menu-mlid-1156"><a href="http://localhost/academic/calender" class="sf-depth-2">Academic Calender</a></li>
                                                                  <li class="leaf menu-mlid-1294"><a href="http://localhost/academic/holidays" class="sf-depth-2">List of Holidays</a></li>
                                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/ attendance" class="sf-depth-2">Attendance</a></li>
                                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/notice" class="sf-depth-2">Notice From Govt.</a></li>
                                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/rules" class="sf-depth-2">Disciplinary Rules</a></li>
                                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/academiccouncil" class="sf-depth-2">MoM of Academic Council</a></li>
                                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/antiragging" class="sf-depth-2">Anti Ragging</a></li>
                                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/universityresults" class="sf-depth-2">University Result</a></li>
                                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/mentors" class="sf-depth-2">Mentors List</a></li>

                                                                     </ul></div>

                                                                  </section>
                                                                    </div>
                                                                        </aside>

            <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                    <div id="pl-174" class="panel-layout">
                        <div id="pg-174-0" class="panel-grid panel-no-style">
                            <div id="pgc-174-0-0" class="panel-grid-cell" data-weight="1">
                                <div id="panel-174-0-0-0"
                                     class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                     data-index="0">
                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                        <div class="siteorigin-widget-tinymce textwidget"><h3
                                                id="ctl00_ContentPlaceHolder1_h4_heading">Entrance Test</h3>
                                            <div style="font-family: Gorgia, Helvetica, sans-serif; font-size: 16px; text-align: justify;">
                                                <div id="ctl00_ContentPlaceHolder1_div_0"><p>Admission in Katihar Engineering College (KEC) for four years B.Tech. course is made through Bihar Combined Entrance Competitive Examination (B.C.E.C.E.) conducted by Bihar Combined Entrance Competitive Examination Board. The candidates desirous of seeking Admission in KEC are advised to appear in competitive examination of B.C.E.C.E. The details for filling up the form for B.C.E.C.E., fee structure, quota and other things is available in its prospectus as well as on the Website:
                                                    <a href="http://bceceboard.bihar.gov.in/pdf_Pros/PROS_BC18.pdf">http://bceceboard.bihar.gov.in/pdf_Pros/PROS_BC18.pdf</a>
                                                    <p> <a href="http://bceceboard.bihar.gov.in/">http://bceceboard.bihar.gov.in/</a></p>
                                                </p>
                                                    <p id="ctl00_ContentPlaceHolder1_h4_heading"><strong>Minimum Qualification for admission in first year of UG programme</strong></p>
                                                    <div id="ctl00_ContentPlaceHolder1_div_1">The minimum academic qualification for admission through BCECE entrance examination is a pass in the final examination of the Intermediate, 10+2 system or its equivalent. Those appearing in the Intermediate final or equivalent examination may also appear in the entrance examination for consideration of provisional admission. All provisional admissions will stand cancelled, if proof of having passed the qualifying examination (Intermediate or equivalent) is not submitted to the institute at the time of counselling. If an applicant is found ineligible at a later date even after admission to the MIT, his/her admission will be cancelled.
                                                        All admissions will be subject to verification of facts from the original certificates/documents of the candidates. The decision of the B.C.E.C.E. Board regarding eligibility of any applicant shall be final.

                                                    </div>
                                                    <p id="ctl00_ContentPlaceHolder1_h4_heading"><strong>Minimum Qualification for the direct admission in second year of UG programme</strong></p>
                                                    <div id="ctl00_ContentPlaceHolder1_div_1">(A) Passed Diploma examination with at least 45% marks (40% in case of candidates belonging to reserved category) in appropriate branch of Engineering and Technology.
                                                        <p>(B) Passed B.Sc. Degree from a recognized University as defined by UGC, with at least 45% marks (40% in case of candidates belonging to reserved category) and passed 10+2 examination with Mathematics as a subject.
                                                        <p>(C) Provided that the students belonging to B.Sc. Stream, shall clear the subjects Engineering Graphics / Engineering Drawing and Engineering Mechanics of the First year Engineering Programme along with the Second year subjects.
                                                        <p>(D) Provided that the students belonging to B.Sc. Stream shall be considered only after filling the supernumerary seats in this category with students belonging to the Diploma stream


                                                        <div><p><strong>The list of qualifying examinations For Engineering
                                                        Stream</strong></p>
                                                        <ul>
                                                            <li>Intermediate Examination, Bihar Intermediate Education
                                                                Council
                                                            </li>
                                                            <li>The 10+2, Central Board of Secondary Education, New
                                                                Delhi
                                                            </li>
                                                            <li>The 10+2, Council for Indian School Certificate
                                                                Examination, New Delhi.
                                                            </li>
                                                            <li>The +2 level examination in the 10 +2 pattern of
                                                                examination of any recognized Central Board of Secondary
                                                                Examination and Council for Indian School Certificate
                                                                Examination, New Delhi.
                                                            </li>
                                                            <li>3 or 4 year diploma recognized by AICTE or a state board
                                                                of technical education. with Physics, Chemistry &amp;
                                                                Mathematics as combination.
                                                            </li>
                                                        </ul>

                                                        <div data-animation="bounceInUp" data-animation-delay="0s">
                                                            <div id="ctl00_ContentPlaceHolder1_div_2"><strong>Residential</strong>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div data-animation="bounceInUp" data-animation-delay="0s">
                                                                <div id="ctl00_ContentPlaceHolder1_div_3"><p>Only those
                                                                    candidates can get admission who are the citizen of
                                                                    India and</p>
                                                                    <ul>
                                                                        <li style="list-style-type: none;">Their parents
                                                                            are the native of Bihar.
                                                                        </li>
                                                                        <li style="list-style-type: none;">Their parents
                                                                            are domiciled in Bihar.
                                                                        </li>
                                                                        <li style="list-style-type: none;">Their parents
                                                                            are registered refugee of Bihar.
                                                                        </li>
                                                                        <li style="list-style-type: none;">Their parents
                                                                            are native of other states but employee of
                                                                            Govt. of Bihar.
                                                                        </li>
                                                                    </ul>
                                                                    <p><strong>List of course offered at KEC.:-</strong></p>
                                                                    <p><span
                                                                            style="font-family: Georgia, Palatino;"><strong>Branch &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspIntake</strong></span>
                                                                    </p>
                                                                    <p><span style="font-family: Georgia, Palatino;">Civil Engineering&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp:60+12*</span>
                                                                    </p>
                                                                    <p><span style="font-family: Georgia, Palatino;">Mechanical Engineering&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp:             60+12*</span>
                                                                    </p>

                                                                    <p><span style="font-family: Georgia, Palatino;">Computer Sc. & Engineering&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp:60 +12*</span>

                                                                    </p>
                                                                    <p><code>*Admisssion through lateral Entry</p></div></code>
                                                            </div>
                                                        </div>
                                                    </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="/jsp/footer.jsp"/>
