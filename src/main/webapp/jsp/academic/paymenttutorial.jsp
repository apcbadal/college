<jsp:include page="/jsp/header.jsp"/>
     <div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
             <div class="container">
                 <div class="row">
                     <div class="col-md-10 col-sm-8"><h3>Fee Payment ( Through SBI COLLECT)</h3></div>
                 </div>
             </div>
         </div>
         <div class="container">



             <div class="row">
                 <aside class="col-sm-3" role="complementary">
                                                                                 <div class="region region-sidebar-second">
                                                                       <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                                               <h2 class="block-title">Academics</h2>

                                                                         <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                                                        <ul class="menu nav">
                                                                       <li class="first leaf  menu-mlid-1154"><a href="http://localhost/academic/admission" class="sf-depth-2 active">UG Admission</a></li>
                                                                       <li class="leaf menu-mlid-1121"><a href="http://localhost/academic/regulations" class="sf-depth-2">Academic Regulations</a></li>
                                                                       <li class="leaf menu-mlid-1156"><a href="http://localhost/academic/calender" class="sf-depth-2">Academic Calender</a></li>
                                                                       <li class="leaf menu-mlid-1294"><a href="http://localhost/academic/holidays" class="sf-depth-2">List of Holidays</a></li>
                                                                       <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/ attendance" class="sf-depth-2">Attendance</a></li>
                                                                       <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/notice" class="sf-depth-2">Notice From Govt.</a></li>
                                                                       <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/rules" class="sf-depth-2">Disciplinary Rules</a></li>
                                                                       <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/academiccouncil" class="sf-depth-2">MoM of Academic Council</a></li>
                                                                       <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/antiragging" class="sf-depth-2">Anti Ragging</a></li>
                                                                       <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/universityresults" class="sf-depth-2">University Result</a></li>
                                                                       <li class="leaf menu-mlid-1467"><a href="http://localhost/academic/mentors" class="sf-depth-2">Mentors List</a></li>

                                                                          </ul></div>

                                                                       </section>
                                                                         </div>
                                                                             </aside>

                 <div class="col-md-8">
                     <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                         <div id="pl-174" class="panel-layout">
                             <div id="pg-174-0" class="panel-grid panel-no-style">
                                 <div id="pgc-174-0-0" class="panel-grid-cell" data-weight="1">
                                     <div id="panel-174-0-0-0"
                                          class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                          data-index="0">
                                         <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                             <div class="siteorigin-widget-tinymce textwidget"><h3
                                                     id="ctl00_ContentPlaceHolder1_h4_heading">Steps To Be Followed For Fee Payment:</h3><br/>
                                                 <div style="font-family: Gorgia, Helvetica, sans-serif; font-size: 16px; text-align: justify;">
                                                     <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Step 1: </strong>Open any browser.</div>
                                                  <br/>   <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Step 2: </strong>Go to the link <strong>onlinesbi.com/sbicollect</strong> a page of "terms" will be opened.</div>
                                                  <br/>    <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Step 3: Tick </strong>the checkbox(which is the acception of terms and conditions) after that click on <strong>proceed.</strong> A new page will be opened.</div>
                                                 <br/>     <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Step 4: </strong>After that Select Your  <strong>State of Corporate/Institution</strong> (i.e; Bihar) and <strong>Type of Corporate/Institution</strong> (i.e; Educational Institutions) and click on <strong>Go</strong>. A new page wii be opened.</div>
                                                    <br/>   <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Step 5: </strong>Now select your <strong>Educational Institutions Name</strong> (i.e; Katihar Engineering College) and submit. A new page will be opened with the logo of katihar Engineering College.  </div>
                                                     <br/>  <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Step 6: </strong>Now <strong>Select Payment Category</strong> (i.e; either fees or fine depending upon for which you are paying.)</div>
                                                  <br/>   <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Step 7: </strong>After that enter your details: Your Name, Registration no., Branch, Category, Email Id, Year/Semester and fees(Corresonding to the fee column which you are paying for).</div>
                                                 <br/>    <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Step 8: </strong>After that again enter your Name,Date of Birth/Incorporation, Mobile no., Email Id and the text shown in the image for banking and <strong>Submit</strong>.</div>
                                                <br/>     <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Step 9: </strong>After that a page with the details entered by you gets opened. Check the details carefully, if correct then <strong>Submit</strong> </div>
                                                 <br/>    <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Step 10: </strong>Now a page with  several payment modes will be opened. Select the payment mode for which you are able and complete the payment.</div>
                                                 <br/>    <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Step 11: </strong>When you are done with payment you can download the e-Receipt of payment.</div>
<br/>
                                                     <div id="ctl00_ContentPlaceHolder1_div_0"><strong>Or you can also download the the pdf which contains the screenshots of the steps as mentioned above for fee payment: </strong>
                                                                                                                                             <a href="http://localhost/jsp/facilities/payment.pdf" target="_blank">Click here to download</a></div>


                                                 </div>
                                             </div>

                                          </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>

              </div>
          </div>
      </div>













    <jsp:include page="/jsp/footer.jsp"/>
