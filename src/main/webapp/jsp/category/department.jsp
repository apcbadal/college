<jsp:include page="/jsp/header.jsp"/>
<aside class="col-sm-3" role="complementary"style="margin-left:30%">
          <div class="region region-sidebar-second">
<section id="block-menu-block-3" class="block block-menu-block clearfix">

        <h2 class="block-title">In this section</h2>

  <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
 <ul class="menu nav">
<li class="first leaf  menu-mlid-1154"><a href="http://localhost/department/civil/about" class="sf-depth-2 active">Civil Engineering</a></li>
<li class="leaf menu-mlid-1121"><a href="http://localhost/department/mechanical/mech" class="sf-depth-2">Mechanical Engineering</a></li>
<li class="leaf menu-mlid-1156"><a href="http://localhost/department/eee/about" class="sf-depth-2">Electrical & Electronics Engineering</a></li>
<li class="leaf menu-mlid-1294"><a href="http://localhost/department/cse/about" class="sf-depth-2">Computer Sc. & Engineering</a></li>
<li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/about" class="sf-depth-2">Applied Sc. & Humanities</a></li>

</ul></div>

</section>
  </div>
      </aside>  <!-- /#sidebar-second -->

<jsp:include page="/jsp/footer.jsp"/>
