<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div style="margin-left:40px"><h3>KEC Web Team</h3></div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-md-4">
                <ul class="list-group page-menu"></ul>
            </div>
            <div class="col-md-8">
                <div class="page-content"><h2>List of Website Developers</h2>
                    <hr/>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Md Talib Ahmad</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/jsp/category/mta.png"
                                                                              style="border-radius:50%"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Computer Science and
                                    Engineering<br/> <strong>Designation:</strong> Assistant Professor & KEC Web
                                    Head<br/> <strong>Email
                                        Address:</strong> <a href="mailto:talib@keck.ac.in">talib@keck.ac.in</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/cse/mta"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Sinjan Kumar</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/jsp/category/snjk.png"
                                                                             style="border-radius:50%"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Assistant
                                    Professor & Content Head<br/> <strong>Phone Number:</strong>+91-9871634888<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                             data-cfemail="03756a69627a436e6a776e767962656562717376712d6c7164">sinjan.dtu@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/cse/snjk"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Ankit Dutta&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Student(2K16)</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/jsp/category/ad.jpg"
                                                                             style="border-radius:50%"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong>Web developer &
                                    Content Writer<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                             data-cfemail="aad9cbc4dec5d9c2d8cbc3eac7c3dec7dfd0cbcccccbd8dadfd884c5d8cd">ankitdutta170@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Anshuman Pd. Choudhary Badal &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Student(2K16)</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="https://kecwebdeploy.firebaseapp.com/images/anshu.jpg"
                                                                             style="border-radius:50%"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Web developer &
                                    UI designer<br/> <strong>Phone Number:</strong>+91-7091177725<br/> <strong>Email
                                        Address:</strong> <a href="mailto:apcbadal@gmail.com" class="__cf_email__"
                                                             data-cfemail="03756a69627a436e6a776e767962656562717376712d6c7164">apcbadal@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                    
                                </div><div class="col-sm-3 box-profile-link"><a href="https://apcbadal.github.io"target="_blank"class="btn btn-default btn-lg"> <br
                                                                              class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                                                          Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Gaurav Kumar (Shyam)&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Student(2K16)</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="https://slackanshu.web.app/images/SAVE_20200715_152559.jpg"
                                                                             style="border-radius:50%"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Web developer &
                                    Visual Designer<br/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Suraj Kumar Sah&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Student(2K16)</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/jsp/category/suraj.png"
                                                                             style="border-radius:50%"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Web Designer &
                                    Content Editor<br/> <strong>Phone Number:</strong>+91-9199730700<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                             data-cfemail="03756a69627a436e6a776e767962656562717376712d6c7164">surajkumarsah165@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Aman Kumar Pandit &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Student(2K16)</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/jsp/category/aman.png"
                                                                             style="border-radius:50%"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> UI/UX
                                    Designer<br/> <strong>Phone Number:</strong>+91-7004586662<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                             data-cfemail="dba8b3acbeafba9bb6b2afb6aea1babdbdbaa9abaea9f5b4a9bc">amankumarpandit22@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<jsp:include page="/jsp/footer.jsp"/>
