<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Principal&#8217;s Message</h3></div>

            </div>
        </div>
    </div>
    <div class="container">

            <a href="/"><i class="fa fa-home"></i> Home</a>


        <div class="row">
            <aside class="col-sm-3" role="complementary">
                                                        <div class="region region-sidebar-second">
                                              <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                      <h2 class="block-title">About KEC</h2>

                                                <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                               <ul class="menu nav">
                                              <li class="first leaf  menu-mlid-1154"><a href="http://localhost/about/principal" class="sf-depth-2 active">Principal's Message</a></li>
                                              <li class="leaf menu-mlid-1121"><a href="http://localhost/about/vision" class="sf-depth-2">Vision & Mission</a></li>
                                              <li class="leaf menu-mlid-1156"><a href="http://localhost/about/allotment" class="sf-depth-2">Allotment &Surrender Report</a></li>
                                              <li class="leaf menu-mlid-1294"><a href="http://localhost/about/affiliation" class="sf-depth-2">Affiliation</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/about/administration" class="sf-depth-2">Administration</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/about/staff" class="sf-depth-2">Support Staff</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/about/history" class="sf-depth-2">History</a></li>

                                                 </ul></div>

                                              </section>
                                                </div>
                                                    </aside>

            <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;"><p
                        style="text-align: center;"><strong>Principal&#8217;s Message</strong></p>
                    <div style="font-family: Georgia, Helvetica, sans-serif; font-size: 16px; text-align: justify;">
                        <div style="float: left;"><img class=" wp-image-81 alignleft"
                                                       style="border-radius: 50%; background-image: url('http://localhost/11/image${fileExtension}'); background-position: center center; background-size: cover; width: 202px; height: 202px;"
                                                       src="http://localhost/11/image${fileExtension}"
                                                       alt=""/></div>
                        <div style="padding: 25px; margin-left: 10px;" data-animation="bounceInUp"
                             data-animation-delay="0s">Education comes from within, you get it by struggle, effort and thought.<p><strong> We cannot always build the future for our students, but we can build our students for the future</strong></p>At KEC Katihar we would like to focus on grooming young minds as the pillar of advancement of a nation & hope of the future. we believe in humanity, discipline, punctuality, regularity. Words visualize the ambition and our aim is to educate the students to LEARN, not just STUDY.<p>Hard work and discipline are the sure roads towards success. Therefore Our doctrine of student's development is based on three Ds - Determination, Dedication and Discipline so that they can excel in their respective fields. Our main job is not only to get them jobs instead; facilitate them to provide jobs. Ergo we strive to endeavour beyond the boundaries of mere books.</p>So, it's my pleasure to welcome you to KEC Katihar,an institute under department of science and technology, government of Bihar . We ensure that the students prove themselves to be not only well qualified engineers but also very responsible and ideal citizens of our country.</div>
                        <hr/>
                        <p>With Regards,<br/> <strong>Smt. Ranjana Kumari </strong><br/> Email: <a
                                href="mailto:principalranjanakri84@gmail.com" >principalranjanakri84@gmail.com</a>
                        </p></div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="/jsp/footer.jsp"/>
