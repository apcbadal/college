<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Vision and Mission</h3></div>

            </div>
        </div>
    </div>
    <div class="container">

            <a href="/"><i class="fa fa-home"></i> Home</a>

        </ul>
        <div class="row">
             <aside class="col-sm-3" role="complementary">
                                                         <div class="region region-sidebar-second">
                                               <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                       <h2 class="block-title">About KEC</h2>

                                                 <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                                <ul class="menu nav">
                                               <li class="first leaf  menu-mlid-1154"><a href="http://localhost/about/principal" class="sf-depth-2 active">Principal's Message</a></li>
                                               <li class="leaf menu-mlid-1121"><a href="http://localhost/about/vision" class="sf-depth-2">Vision & Mission</a></li>
                                               <li class="leaf menu-mlid-1156"><a href="http://localhost/about/allotment" class="sf-depth-2">Allotment &Surrender Report</a></li>
                                               <li class="leaf menu-mlid-1294"><a href="http://localhost/about/affiliation" class="sf-depth-2">Affiliation</a></li>
                                               <li class="leaf menu-mlid-1467"><a href="http://localhost/about/administration" class="sf-depth-2">Administration</a></li>
                                               <li class="leaf menu-mlid-1467"><a href="http://localhost/about/staff" class="sf-depth-2">Support Staff</a></li>
                                               <li class="leaf menu-mlid-1467"><a href="http://localhost/about/history" class="sf-depth-2">History</a></li>

                                                  </ul></div>

                                               </section>
                                                 </div>
                                                     </aside>

            <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                    <div id="pl-482" class="panel-layout">
                        <div id="pg-482-0" class="panel-grid panel-no-style">
                            <div id="pgc-482-0-0" class="panel-grid-cell" data-weight="1">
                                <div id="panel-482-0-0-0"
                                     class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                     data-index="0"
                                     data-style="{&quot;background&quot;:&quot;#a5d689&quot;,&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;,&quot;font_color&quot;:&quot;#000000&quot;,&quot;link_color&quot;:&quot;#8224e3&quot;}">
                                    <div class="panel-widget-style panel-widget-style-for-482-0-0-0">
                                        <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                            <div class="siteorigin-widget-tinymce textwidget"><p
                                                    style="text-align: center;"><span
                                                    style="text-decoration: underline; font-family: georgia, palatino; font-size: large; color: #800000;"><b>Vision</b></span>
                                            </p>
                                                <li style="padding-left: 30px; text-align: justify;"><span
                                                        style="font-family: georgia, palatino; font-size: medium;">To provide education in Engineering with excellence and ethics and to reach the unreached.</span>
                                                </li>
                                                <li style="padding-left: 30px; text-align: justify;"><span
                                                        style="font-family: georgia, palatino; font-size: medium;">To serve the nation and society by providing skilled and well developed human resource through &nbsp&nbsp&nbsp&nbsp&nbsp excellence in technical education and research.</span>
                                                </li>
                                                <li style="padding-left: 30px; text-align: justify;"><span
                                                        style="font-family: georgia, palatino; font-size: medium;">
                                                To be a leading institution of engineering education and research</span></li>

                                                <hr/>
                                                <p style="text-align: center;"><span
                                                        style="text-decoration: underline; font-family: georgia, palatino; font-size: large; color: #800000;"><b>Mission</b></span>
                                                </p>
                                                <ul>
                                                    <li style="text-align: justify;"><span
                                                            style="font-family: georgia, palatino; font-size: medium;">To build across the institute a culture of excellence in teaching and learning with needed performance and accountability from all support activities.
                                                    </li>
                                                    <li style="text-align: justify;"><span
                                                            style="font-family: georgia, palatino; font-size: medium;">To become center of excellence in technical education and research and to occupy a place amongst eminent institutions of the nation.
                                                    </li>
                                                    <li style="text-align: justify;"><span
                                                            style="font-family: georgia, palatino; font-size: medium;">To create innovative and vibrant young leaders and entrepreneurs in Engineering and Technology for building India as a super knowledge power and blossom into a University of excellence recognized globally.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="/jsp/footer.jsp"/>
