<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Affiliation</h3></div>

            </div>
        </div>
    </div>
    <div class="container">
        <a href="/"><i class="fa fa-home"></i> Home</a>
        <div class="row">

             <aside class="col-sm-3" role="complementary">
                                                         <div class="region region-sidebar-second">
                                               <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                       <h2 class="block-title">About KEC</h2>

                                                 <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                                <ul class="menu nav">
                                               <li class="first leaf  menu-mlid-1154"><a href="http://localhost/about/principal" class="sf-depth-2 active">Principal's Message</a></li>
                                               <li class="leaf menu-mlid-1121"><a href="http://localhost/about/vision" class="sf-depth-2">Vision & Mission</a></li>
                                               <li class="leaf menu-mlid-1156"><a href="http://localhost/about/allotment" class="sf-depth-2">Allotment &Surrender Report</a></li>
                                               <li class="leaf menu-mlid-1294"><a href="http://localhost/about/affiliation" class="sf-depth-2">Affiliation</a></li>
                                               <li class="leaf menu-mlid-1467"><a href="http://localhost/about/administration" class="sf-depth-2">Administration</a></li>
                                               <li class="leaf menu-mlid-1467"><a href="http://localhost/about/staff" class="sf-depth-2">Support Staff</a></li>
                                               <li class="leaf menu-mlid-1467"><a href="http://localhost/about/history" class="sf-depth-2">History</a></li>

                                                  </ul></div>

                                               </section>
                                                 </div>
                                                     </aside>

            <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                    <div data-animation="bounce" data-animation-delay="0s"><h3
                            id="ctl00_ContentPlaceHolder1_h4_heading">Affiliation</h3></div>
                    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; text-align: justify;">
                        <div data-animation="bounceInUp" data-animation-delay="0s">
                            <div id="ctl00_ContentPlaceHolder1_div_4">The Institute is academically governed by <a
                                    id="ctl00_ContentPlaceHolder1_hl_aryabhatta" href="http://www.akubihar.ac.in/"
                                    target="_blank" rel="noopener">Aryabhatta Knowlege University, Patna</a> which is
                                the degree awarding authority for the institute as B.Tech. is recognized by the
                                A.I.C.T.E., New Delhi, and the Government of Bihar State.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="/jsp/footer.jsp"/>
