<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Support Staff</h3></div>

            </div>
        </div>
    </div>
    <div class="container">
        <a href="/"><i class="fa fa-home"></i> Home</a>
        <div class="row">
            <aside class="col-sm-3" role="complementary">
                                                        <div class="region region-sidebar-second">
                                              <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                      <h2 class="block-title">About KEC</h2>

                                                <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                               <ul class="menu nav">
                                              <li class="first leaf  menu-mlid-1154"><a href="http://localhost/about/principal" class="sf-depth-2 active">Principal's Message</a></li>
                                              <li class="leaf menu-mlid-1121"><a href="http://localhost/about/vision" class="sf-depth-2">Vision & Mission</a></li>
                                              <li class="leaf menu-mlid-1156"><a href="http://localhost/about/allotment" class="sf-depth-2">Allotment &Surrender Report</a></li>
                                              <li class="leaf menu-mlid-1294"><a href="http://localhost/about/affiliation" class="sf-depth-2">Affiliation</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/about/administration" class="sf-depth-2">Administration</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/about/staff" class="sf-depth-2">Support Staff</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/about/history" class="sf-depth-2">History</a></li>

                                                 </ul></div>

                                              </section>
                                                </div>
                                                    </aside>

            <div class="col-md-8">
                <div class="page-content"><h2>List of Support Staffs</h2>
                    <hr/>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Rahul Kumar </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/about-kec/rk.png"
                                                                             style="background-image: url ('http://localhost/about-kec/rk.png')"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Mechanical
                                    Engineering<br/> <strong>Designation:</strong> Lab Assistant
                                    <br/> <strong>Phone Number:</strong> +91-7903327337<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                             data-cfemail="98f9ebf0f1ebf0d8f5f1ecf5ede2f9fefef9eae8edeab6f7eaff">singhrahul0312@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                                            <div class="panel-heading faculty-title">Gaurav Kumar</div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/about-kec/gk.jpg"
                                                                                                 style="background-image: url(http://localhost/about-kec/bs.jpg)"/>
                                                        <div class="visible-xs"><br/></div>
                                                    </div>
                                                    <div class="col-sm-6 box-profile-info"> <strong>Designation:</strong>Office Assistant
                                                       <br/> <strong>Phone Number:</strong>+91-1234567890<br/> <strong>Email
                                                            Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                                                 data-cfemail="03756a69627a436e6a776e767962656562717376712d6c7164">abc@gmail.com</a>
                                                        <div class="visible-xs"><br/></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Bhumeshwar Singh</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/about-kec/bs.jpg"
                                                                             style="background-image: url(http://localhost/about-kec/bs.jpg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"> <strong>Designation:</strong>Data Entry Operator
                                   <br/> <strong>Phone Number:</strong>+91-7250404460<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                             data-cfemail="03756a69627a436e6a776e767962656562717376712d6c7164">bhumeshwarsingh85@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Md Taufique Alam</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/about-kec/data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                                                             style="background-image: url(http://localhost/about-kec/mta.jpg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Designation:</strong>Data Entry Operator<br/> <strong>Phone Number:</strong>+91-7488686325<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                             data-cfemail="aad9cbc4dec5d9c2d8cbc3eac7c3dec7dfd0cbcccccbd8dadfd884c5d8cd">tralam345@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Md Maqsood </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/about-kce/cmm.jpeg"
                                                                             style="background-image: url(http://localhost/about-kec/mm.jpeg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong>Lab Assistant<br/> <strong>Qualification:</strong>B.Tech(Electronics & Communication)<br/> <strong>Phone Number:</strong>+91-9097010373<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                             data-cfemail="03756a69627a436e6a776e767962656562717376712d6c7164">engg.maqsood@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Ranjeet Sinha</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/about-kec/data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                                                             style="background-image: url(http://localhost/about-kec/rs.jpeg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Designation:</strong>Office Attendant
                                    <br/> <strong>Phone Number:</strong>+91-7485003613<br/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Manilal Manjhi</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/about-kec/data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                                                             style="background-image: url(http://localhost/about-kec/mani.jpeg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"> <strong>Designation:</strong>Office Attendant <br/> <strong>Phone Number:</strong> 6207310940<br/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Arvind Kumar</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/about-kec/data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                                                             style="background-image: url(http://localhost/about-kec/dummy.png)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Designation:</strong>Clerk
                                    <br/> <strong>Phone Number:</strong><br/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
   <jsp:include page="/jsp/footer.jsp"/>
