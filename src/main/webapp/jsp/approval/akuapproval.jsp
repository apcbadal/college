<jsp:include page="/jsp/header.jsp" />

 <div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>AKU Approval</h3></div>

            </div>

    <div class="container">
        <div class="row">
            <aside class="col-sm-3" role="complementary">
                 <div class="region region-sidebar-second">
                   <section id="block-menu-block-3" class="block block-menu-block clearfix">
                      <h2 class="block-title">Approval</h2>
                     <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                         <ul class="menu nav">
                             <li class="first leaf  menu-mlid-1154"><a href="http://localhost/approval/aicteapproval" class="sf-depth-2 active">AICTE Approval</a></li>
                               <li class="leaf menu-mlid-1121"><a href="http://localhost/approval/akuapproval" class="sf-depth-2">AKU Approval</a></li>
                          </ul>
                     </div>
                   </section>
                 </div>
                </aside>

            <div class="col-md-8 col-md-offset-2">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300; margin-left:700 ">
                    <p>
                        <embed src="http://localhost/jsp/approval/aku.pdf" width="700px" height="700px"></embed>
                    </p>
                     <p><strong>AKU Regulation &#8212;-</strong></p>
                    <i class="fa fa-download"></i>
                       <a href="http://localhost/jsp/approval/aku.pdf">
                       <button type="button" class="btn btn-success" data-dismiss="modal">Download</button></a>
            </div>
        </div>
    </div>
</div>
<jsp:include page="/jsp/footer.jsp" />
