<jsp:include page="/jsp/header.jsp" />


    <div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3 style="margin-left: 40%;">Students Placement Coordinator</h3></div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <aside class="col-sm-3" role="complementary">
                                    <div class="region region-sidebar-second">
                                      <section id="block-menu-block-3" class="block block-menu-block clearfix">
                                         <h2 class="block-title">Training & Placement </h2>
                                        <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                            <ul class="menu nav">
                                                <li class="first leaf  menu-mlid-1154"><a href="http://localhost/trainingplacement/aboutplacement" class="sf-depth-2 active">About Placement</a></li>
                                                  <li class="leaf menu-mlid-1121"><a href="http://localhost/trainingplacement/rulesoftrainingplacement" class="sf-depth-2">Rules of Training & Placement</a></li>
                                                  <li class="leaf menu-mlid-1121"><a href="http://localhost/trainingplacement/tipsforresume" class="sf-depth-2">Tips for Resume</a></li>
                                                   <li class="leaf menu-mlid-1121"><a href="http://localhost/trainingplacement/placementbrochure" class="sf-depth-2">Placement Brochure</a></li>
                                             </ul>
                                        </div>
                                      </section>
                                      </div>
                                   </aside>

            <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;margin-left: 40%;">
                    <table border="1">
                        <tbody>
                        <tr>
                            <td width="42"><span style="color: #000000;">SNO</span></td>
                            <td width="168"><span style="color: #000000;">NAME</span></td>
                            <td width="102"><span style="color: #000000;">DEPARTMENT</span></td>
                            <td width="245"><span style="color: #000000;">EMAIL ID</span></td>
                            <td width="245"><span style="color: #000000;">Mob Num</span></td>
                        </tr>
                         <tr>
                                                    <td width="42"><span style="color: #000000;">1</span></td>
                                                    <td width="168"><span style="color: #000000;">Abhishek Asit</span></td>
                                                    <td width="102"><span style="color: #000000;">ME</span></td>
                                                    <td width="245"><span style="color: #000000;"></span></td>
                                                    <td width="245"><span style="color: #000000;">9631178799</span></td>
                                                </tr>
                          <tr>
                                                     <td width="42"><span style="color: #000000;">2</span></td>
                                                     <td width="168"><span style="color: #000000;">Ankit Dutta</span></td>
                                                     <td width="102"><span style="color: #000000;">CSE</span></td>
                                                     <td width="245"><span style="color: #000000;">ankitdutta170@gmail.com</span></td>
                                                     <td width="245"><span style="color: #000000;"></span></td>
                                                 </tr>
                           <tr>
                                                      <td width="42"><span style="color: #000000;">3</span></td>
                                                      <td width="168"><span style="color: #000000;">Mustaqueem Diwan</span></td>
                                                      <td width="102"><span style="color: #000000;">CE</span></td>
                                                      <td width="245"><span style="color: #000000;"></span></td>
                                                      <td width="245"><span style="color: #000000;"></span></td>
                          </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<jsp:include page="/jsp/footer.jsp" />
