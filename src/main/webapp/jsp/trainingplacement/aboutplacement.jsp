<jsp:include page="/jsp/header.jsp" />

<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>About Placement</h3></div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
<aside class="col-sm-3" role="complementary">
                        <div class="region region-sidebar-second">
                          <section id="block-menu-block-3" class="block block-menu-block clearfix">
                             <h2 class="block-title">Training & Placement </h2>
                            <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                <ul class="menu nav">
                                    <li class="first leaf  menu-mlid-1154"><a href="http://localhost/trainingplacement/aboutplacement" class="sf-depth-2 active">About Placement</a></li>
                                      <li class="leaf menu-mlid-1121"><a href="http://localhost/trainingplacement/rulesoftrainingplacement" class="sf-depth-2">Rules of Training & Placement</a></li>
                                      <li class="leaf menu-mlid-1121"><a href="http://localhost/trainingplacement/tipsforresume" class="sf-depth-2">Tips for Resume</a></li>
                                       <li class="leaf menu-mlid-1121"><a href="http://localhost/trainingplacement/placementbrochure" class="sf-depth-2">Placement Brochure</a></li>
                                 </ul>
                            </div>
                          </section>
                          </div>
                       </aside>
                    <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                    <div style="font-family: georgia, sans-serif; font-size: 16px; text-align: justify;"><p
                            id="ctl00_ContentPlaceHolder1_h4_heading"><span
                            style="font-family: Georgia, Palatino;"><strong>About T&P Cell</strong></span></p>
                        <div id="ctl00_ContentPlaceHolder1_div_3">The Training and Placement Office provides job opportunities, arranges Campus Interviews by various Companies of National and International repute. Students take the help of this office for their Training and Employment. It also caters to the need of Instructional and Industrial requirements through state of the art facilities, new technologies and human resources in collaboration with all the Department of studies.
The Training & Placement cell attempts to prepare the students for the career opportunities in the area of engineering and technology. The Training & Placement cell arranges training programs to groom the students to their best for the corporate responsibilities and make them employable and industry ready. We strive for mutual industry-institute relationship by involving institute with industry in several activities such as campus placements process, internship hiring, industrial visits, on-site training, expert lectures, workshops and seminars.
<span style="font-family: Georgia, Palatino;"></span><br/>
                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                                            <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300; ">
                                                <p>
                                                    <embed align="center" src="http://localhost/jsp/trainingplacement/brochure.pdf" width="700px" height="750px"></embed>
                                                </p>
                                            </div>
                                        </div>
            </div>
        </div>
    </div>

<jsp:include page="/jsp/footer.jsp" />
