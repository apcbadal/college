<jsp:include page="/jsp/header.jsp" />


    <div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Rules of Training &#038; Placement</h3></div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <aside class="col-sm-3" role="complementary">
                                    <div class="region region-sidebar-second">
                                      <section id="block-menu-block-3" class="block block-menu-block clearfix">
                                         <h2 class="block-title">Training & Placement </h2>
                                        <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                            <ul class="menu nav">
                                                <li class="first leaf  menu-mlid-1154"><a href="http://localhost/trainingplacement/aboutplacement" class="sf-depth-2 active">About Placement</a></li>
                                                  <li class="leaf menu-mlid-1121"><a href="http://localhost/trainingplacement/rulesoftrainingplacement" class="sf-depth-2">Rules of Training & Placement</a></li>
                                                  <li class="leaf menu-mlid-1121"><a href="http://localhost/trainingplacement/tipsforresume" class="sf-depth-2">Tips for Resume</a></li>
                                                   <li class="leaf menu-mlid-1121"><a href="http://localhost/trainingplacement/placementbrochure" class="sf-depth-2">Placement Brochure</a></li>
                                             </ul>
                                        </div>
                                      </section>
                                      </div>
                                   </aside>

            <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                    <p>
                        <embed src="http://localhost/jsp/trainingplacement/sample.pdf" width="700px" height="850px"></embed>
                    </p>
                </div>
            </div>
        </div>
    </div>

<jsp:include page="/jsp/footer.jsp" />
