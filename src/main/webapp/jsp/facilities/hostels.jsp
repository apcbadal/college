<jsp:include page="/jsp/header.jsp"/>


<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-8"><h3>Hostels</h3></div>
</div>
    </div>
</div>
<div class="container">
    <div class="row">
       <aside class="col-sm-3" role="complementary">
                                                            <div class="region region-sidebar-second">
                                                  <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                          <h2 class="block-title">Facilities </h2>

                                                    <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                                   <ul class="menu nav">
                                                  <li class="first leaf  menu-mlid-1154"><a href="http://localhost/facilities/bank" class="sf-depth-2 active">Bank</a></li>
                                                  <li class="leaf menu-mlid-1121"><a href="http://localhost/facilities/csecenter" class="sf-depth-2">Computer Center</a></li>
                                                  <li class="leaf menu-mlid-1156"><a href="http://localhost/facilities/centrallibrary" class="sf-depth-2">Central Library</a></li>
                                                  <li class="leaf menu-mlid-1294"><a href="http://localhost/facilities/hostels" class="sf-depth-2">Hostels</a></li>
                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/sportsfacilities" class="sf-depth-2">Sports Facilities</a></li>
                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/clubs" class="sf-depth-2">Clubs</a></li>
                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/healthcenter" class="sf-depth-2">Health Center</a></li>
                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/wifi" class="sf-depth-2">Wi Fi</a></li>
                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/medicalfacilities" class="sf-depth-2">Medical Facilities</a></li>
                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/startupcell" class="sf-depth-2">Startup Cell</a></li>
                                                  <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/gymnasium" class="sf-depth-2">Gymnasium</a></li>

                                                     </ul></div>

                                                  </section>
                                                    </div>
                                                        </aside>

        <div class="col-md-8">
            <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                <div style="font-family: georgia, sans-serif; font-size: 16px; text-align: justify;"><h3
                        id="ctl00_ContentPlaceHolder1_h4_heading">Hostels</h3>
                    <p> Currently Katihar Engineering college is not providing any hostel or transportation facilities.
                    Probably from next year ,hostel may be available for students.</p>
                    <p><strong>The administration of hostels is under the supervision of following
                        professors:-</strong></p>


                    <table style="width: 100%;" border="1" align="center" bgcolor="#7eec35">
                        <tbody>
                        <tr bgcolor="#7eec35">
                            <th style="text-align: left;" scope="col"><a>Hostel</a></th>
                            <th style="text-align: left;" scope="col"><a>Warden</a></th>
                            <th style="text-align: left;" scope="col"><a>Contact No</a></th>
                        </tr>
                        <tr bgcolor="#e9feea">
                            <td>Chief Warden</td>
                            <td>Sneha Kumari</td>
                            <td>8405016439</td>
                        </tr>
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
</div>

<jsp:include page="/jsp/footer.jsp"/>
