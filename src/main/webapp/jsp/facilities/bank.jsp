<jsp:include page="/jsp/header.jsp"/>


<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-8"><h3>Banks</h3></div>

        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <!--<div class="col-md-4">
            <ul class="list-group page-menu">
                <li class="list-group-item" style="background: #eee; font-size: 20px;">Facilities and Services</li>
                <li class="list-group-item"><a href="http://localhost/facilities/facilities/facilities/csecenter">&nbsp;&nbsp; <i
                        class="fa fa-angle-double-right"></i> Computer Centre</a></li>
                <li class="list-group-item"><a href="http://localhost/facilities/facilities/centrallibrary">&nbsp;&nbsp; <i
                        class="fa fa-angle-double-right"></i> Central Library</a></li>
                <li class="list-group-item"><a href="http://localhost/facilities/facilities/hostels">&nbsp;&nbsp; <i
                        class="fa fa-angle-double-right"></i> Hostels</a></li>
                <li class="list-group-item"><a href="http://localhost/facilities/facilities/sportsfacilities">&nbsp;&nbsp; <i
                        class="fa fa-angle-double-right"></i> Sports Facilities</a></li>
                <li class="list-group-item"><a href="http://localhost/facilities/facilities/club">&nbsp;&nbsp; <i
                        class="fa fa-angle-double-right"></i> Clubs</a></li>
                <li class="list-group-item "><a href="http://localhost/facilities/facilities/healthcenter">&nbsp;&nbsp; <i
                        class="fa fa-angle-double-right"></i>Health Centre</a></li>
                <li class="list-group-item"><a href="http://localhost/facilities/facilities/wifi">&nbsp;&nbsp; <i
                        class="fa fa-angle-double-right"></i> Wifi</a></li>
                <li class="list-group-item"><a href="http://localhost/facilities/facilities/medicalfacilities">&nbsp;&nbsp; <i
                        class="fa fa-angle-double-right"></i> Medical Facilities</a></li>
                <li class="list-group-item"><a href="http://localhost/facilities/facilities/startupcell">&nbsp;&nbsp; <i
                        class="fa fa-angle-double-right"></i> Startup Cell</a></li>
            </ul>
        </div>-->
        <aside class="col-sm-3" role="complementary">
                                                     <div class="region region-sidebar-second">
                                           <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                   <h2 class="block-title">Facilities </h2>

                                             <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                            <ul class="menu nav">
                                           <li class="first leaf  menu-mlid-1154"><a href="http://localhost/facilities/bank" class="sf-depth-2 active">Bank</a></li>
                                           <li class="leaf menu-mlid-1121"><a href="http://localhost/facilities/csecenter" class="sf-depth-2">Computer Center</a></li>
                                           <li class="leaf menu-mlid-1156"><a href="http://localhost/facilities/centrallibrary" class="sf-depth-2">Central Library</a></li>
                                           <li class="leaf menu-mlid-1294"><a href="http://localhost/facilities/hostels" class="sf-depth-2">Hostels</a></li>
                                           <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/sportsfacilities" class="sf-depth-2">Sports Facilities</a></li>
                                           <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/clubs" class="sf-depth-2">Clubs</a></li>
                                           <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/healthcenter" class="sf-depth-2">Health Center</a></li>
                                           <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/wifi" class="sf-depth-2">Wi Fi</a></li>
                                           <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/medicalfacilities" class="sf-depth-2">Medical Facilities</a></li>
                                           <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/startupcell" class="sf-depth-2">Startup Cell</a></li>
                                           <li class="leaf menu-mlid-1467"><a href="http://localhost/facilities/gymnasium" class="sf-depth-2">Gymnasium</a></li>

                                              </ul></div>

                                           </section>
                                             </div>
                                                 </aside>
        <div class="col-md-8">
                                                    <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300; ">
                                                        <p>
                                                            <table border="1" width="500">
                                                               <tr><td><p>For Detailed Instruction about payment </p></td>
                                                               <td><i class="fa fa-download"></i>
                                                                   <a href="http://localhost/jsp/facilities/payment.pdf">
                                                                   <button type="button" class="btn btn-success" data-dismiss="modal"style="margin-left" class="btn btn-success">click here
                                                                   </button></a>
                                                               </td>
                                                               </tr>
                                                            </table>                                    
                                                                                                
                                                        </p>
                                                    </div>
                                                </div>
                    </div>
    </div>
</div>

<jsp:include page="/jsp/footer.jsp"/>
