<jsp:include page="/jsp/header.jsp"/>

<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Faculty of Mechanical Engg.</h3></div>

            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">

             <aside class="col-sm-3" role="complementary">
                                                          <div class="region region-sidebar-second">
                                                <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                        <h2 class="block-title">In the Department of Mechanical Engg.</h2>

                                                  <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                                 <ul class="menu nav">
                                                <li class="first leaf  menu-mlid-1154"><a href="http://localhost/department/mechanical/mech" class="sf-depth-2 active">About Mechanical Engg.</a></li>
                                                <li class="leaf menu-mlid-1121"><a href="http://localhost/department/mechanical/vision" class="sf-depth-2">Vision & Mission</a></li>
                                                <li class="leaf menu-mlid-1156"><a href="http://localhost/department/mechanical/faculty" class="sf-depth-2">Faculty of Mechanical Engg.</a></li>
                                                <li class="leaf menu-mlid-1294"><a href="http://localhost/department/mechanical/labs" class="sf-depth-2">Labs & Infrastructure</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/mechanical/question" class="sf-depth-2">Question Bank</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/mechanical/awards" class="sf-depth-2">awards & Recognition</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/mechanical/programs" class="sf-depth-2">Program Activities</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/mechanical/students" class="sf-depth-2">Student of Mechanical Engg.</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/mechanical/notice" class="sf-depth-2">Notice</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/mechanical/weekly" class="sf-depth-2">Weekly Test Result</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/mechanical/programs" class="sf-depth-2">Course File</a></li>
                                                 <li class="leaf menu-mlid-1467"><a href="http://localhost/department/mechanical/staff" class="sf-depth-2">Staff of Mechanical Engg.</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/mechanical/attendance" class="sf-depth-2">Attendance Summary</a></li>
                                                   </ul></div>

                                                </section>
                                                  </div>
                                                      </aside>
            <div class="col-md-8">
                <div class="page-content"><h2>Faculty Members</h2>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Dr. Surya Kumar&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<strong>(Head of the Department)</strong></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/26/image${fileExtension}"
                                                                             style="background-image: url(srk.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Mechanical Engineering<br/> <strong>Designation:</strong> Assistant Professor<br/> <strong>Phone Number:</strong> 8650301056<br/> <strong>Email
                                        Address:</strong> <a href="mailto:mechsurya@hotmail.com">mechsurya@hotmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/mechanical/srk"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">  Jayant Kumar</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/27/image${fileExtension}"
                                                                             style="background-image: url(jk.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Mechanical Engineering<br/> <strong>Designation:</strong> Assistant Professor <br/>
                                    <strong>Phone Number:</strong>8755821987<br/> <strong>Email Address:</strong> <a
                                            href="mailto:jayant906@gmail.com">jayant906@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/mechanical/jk" class="btn btn-default btn-lg">
                                    <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Mukesh Kumar</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/28/image${fileExtension}"
                                                                             style="background-image: url(mk.jpeg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Mechanical Engineering<br/> <strong>Designation:</strong> Assistant Professor <br/>
                                    <strong>Phone Number:</strong>9074275324<br/> <strong>Email Address:</strong> <a
                                            href="mailto:mukeshme24@gmail.com">mukeshme24@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/mechanical/mk" class="btn btn-default btn-lg">
                                    <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
					<div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Ajay Kumar</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/29/image${fileExtension}"
                                                                             style="background-image: url(ajk.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Mechanical Engineering<br/> <strong>Designation:</strong> Assistant Professor <br/>
                                    <strong>Phone Number:</strong>8802009023<br/> <strong>Email Address:</strong> <a
                                            href="mailto:ajaykumar2189@gmail.com">ajaykumar2189@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/mechanical/ajk" class="btn btn-default btn-lg">
                                    <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading faculty-title"> Suman Kumar</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3 box-profile-image"><img alt=""
                                                                         src="http://localhost/30/image${fileExtension}"
                                                                         style="background-image: url(smnk.JPG)"/>
                                <div class="visible-xs"><br/></div>
                            </div>
                            <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of Mechanical
                                Engineering<br/> <strong>Designation:</strong> Assistant Professor <br/> <strong>Phone
                                    Number:</strong> 9891024077<br/> <strong>Email Address:</strong> <a
                                        href="mailto:suman828@gmail.com">suman828@gmail.com</a>
                                <div class="visible-xs"><br/></div>
                            </div>
                            <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/mechanical/smnk" class="btn btn-default btn-lg">
                                <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/> Profile
                                Page </a></div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading faculty-title">Dr Arbind Prasad</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/37/image${fileExtension}"
                                                                         style="background-image: url(ap.png)"/>
                                <div class="visible-xs"><br/></div>
                            </div>
                            <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of Mechanical
                                Engineering<br/> <strong>Designation:</strong> Assistant Professor <br/> <strong>Phone
                                    Number:</strong>9678437791<br/> <strong>Email Address:</strong> <a
                                        href="mailto:arbind.geit@gmail.com">arbind.geit@gmail.com</a>
                                <div class="visible-xs"><br/></div>
                            </div>
                            <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/mechanical/ap" class="btn btn-default btn-lg"> <br
                                    class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/> Profile
                                Page </a></div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading faculty-title">Kanchan Kumar Sinha</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/32/image${fileExtension}"
                                                                         style="background-image: url(dummy.png)"/>
                                <div class="visible-xs"><br/></div>
                            </div>
                            <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of Mechanical
                                Engineering<br/> <strong>Designation:</strong> Assistant Professor <br/> <strong>Phone
                                    Number:</strong>${facultyMobNo}<br/> <strong>Email Address:kanchansinha1@gmail.com</strong> <a
                                        href="mailto:kanchansinha1@gmail.com">kanchansinha1@gmail.com</a>
                                <div class="visible-xs"><br/></div>
                            </div>
                            <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/mechanical/kks" class="btn btn-default btn-lg"> <br
                                    class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/> Profile
                                Page </a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<jsp:include page="/jsp/footer.jsp"/>
