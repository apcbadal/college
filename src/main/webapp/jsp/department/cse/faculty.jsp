<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Faculty of Computer Sc. & Engg.</h3></div>

            </div>
        </div>
    </div>
    <div class="container">

        <aside class="col-sm-3" role="complementary">
                              <div class="region region-sidebar-second">
                    <section id="block-menu-block-3" class="block block-menu-block clearfix">

                            <h2 class="block-title">In this section</h2>

                      <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                     <ul class="menu nav">
                    <li class="first leaf  menu-mlid-1154"><a href="http://localhost/department/cse/about" class="sf-depth-2 active">About CSE</a></li>
                    <li class="leaf menu-mlid-1121"><a href="http://localhost/department/cse/vision" class="sf-depth-2">Vision & Mission</a></li>
                    <li class="leaf menu-mlid-1156"><a href="http://localhost/department/cse/faculty" class="sf-depth-2">Faculty of CSE</a></li>
                    <li class="leaf menu-mlid-1294"><a href="http://localhost/department/cse/labs" class="sf-depth-2">Labs & Infrastructure</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/question" class="sf-depth-2">Question Bank</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/awards" class="sf-depth-2">awards & Recognition</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/programs" class="sf-depth-2">Program Activities</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/students" class="sf-depth-2">Student of CSE</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/notice" class="sf-depth-2">Notice</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/weekly" class="sf-depth-2">Weekly Test Result</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/programs" class="sf-depth-2">Course File</a></li>
                     <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/staff" class="sf-depth-2">Staff of CSE</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/attendance" class="sf-depth-2">Attendance Summary</a></li>
                       </ul></div>

                    </section>
                      </div>
                          </aside>

            <div class="col-md-8">
                <div class="page-content"><h2>Faculty Members</h2>
                    <hr/>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Subodh Kumar&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<strong>(Head
                            of the Department)</strong></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/3/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/3/image${fileExtension})"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Computer Science and
                                    Engineering<br/> <strong>Designation:</strong> Assistant Professor<br/> <strong>Phone
                                        Number:</strong>8171778441<br/> <strong>Email Address:</strong> <a
                                            href="mailto:subodhkumar012@gmail.com">subodhkumar012@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/cse/subodh"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Dharmveer Kumar Yadav</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/2/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/cse/dky.jpg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Assistant
                                    Professor<br/> <strong>Phone Number:</strong>8757760847<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection">kumar.dharmveer@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/cse/dky"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Shweta Kumari</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/4/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/cse/Shwetakumari.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Assistant
                                    Professor<br/> <strong>Phone Number:</strong>7549647373<br/> <strong>Email
                                        Address:</strong> <a
                                            href="mailto:shwetaverma673@gmail.com">shwetaverma673@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/cse/shweta"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Sneha Kumari</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/5/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/cse/snehakumari.jpg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Assistant
                                    Professor<br/> <strong>Phone Number:</strong>8405016439<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection">sprasad460@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/cse/snehakumari"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Sujeet Kumar</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/6/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/cse/sjk.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Assistant
                                    Professor<br/> <strong>Phone Number:</strong>9572341301<br/> <strong>Email
                                        Address:</strong> <a
                                            href="/cdn-cgi/l/email-protection">sujeetkrmail@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/cse/sjk"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Md Talib Ahmad</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/1/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/cse/mta.jpg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Assistant
                                    Professor<br/> <strong>Phone Number:</strong>9108006551<br/> <strong>Email
                                        Address:</strong> <a
                                            href="/cdn-cgi/l/email-protection">mdtalibahmad@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/cse/mta"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Pradeep Kumar Sharma</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/7/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/cse/pks.jpg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Assistant
                                    Professor<br/> <strong>Phone Number:</strong>8876024875<br/> <strong>Email
                                        Address:</strong> <a
                                            href="/cdn-cgi/l/email-protection">cspradeepindia@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/cse/pks"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Sinjan Kumar</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/9/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/cse/sjn.jpg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Assistant
                                    Professor<br/> <strong>Phone Number:</strong>9871634888<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection">sinjan.dtu@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/cse/snjk"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Sritosh Kumar</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/10/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/cse/shk.jpg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Computer Science and Engineering<br/> <strong>Designation:</strong> Assistant
                                    Professor<br/> <strong>Phone Number:</strong><br/> <strong>Email
                                        Address:</strong> <a
                                            href="mailto:sritosh04@gmail.com">sritoshkumar04@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/cse/shk"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<jsp:include page="/jsp/footer.jsp"/>
