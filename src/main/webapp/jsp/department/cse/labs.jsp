<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Labs & Infrastructure</h3></div>

            </div>
        </div>
    </div>
    <div class="container">

        <aside class="col-sm-3" role="complementary">
                              <div class="region region-sidebar-second">
                    <section id="block-menu-block-3" class="block block-menu-block clearfix">

                            <h2 class="block-title">In this section</h2>

                      <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                     <ul class="menu nav">
                    <li class="first leaf  menu-mlid-1154"><a href="http://localhost/department/cse/about" class="sf-depth-2 active">About CSE</a></li>
                    <li class="leaf menu-mlid-1121"><a href="http://localhost/department/cse/vision" class="sf-depth-2">Vision & Mission</a></li>
                    <li class="leaf menu-mlid-1156"><a href="http://localhost/department/cse/faculty" class="sf-depth-2">Faculty of CSE</a></li>
                    <li class="leaf menu-mlid-1294"><a href="http://localhost/department/cse/labs" class="sf-depth-2">Labs & Infrastructure</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/question" class="sf-depth-2">Question Bank</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/awards" class="sf-depth-2">awards & Recognition</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/programs" class="sf-depth-2">Program Activities</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/students" class="sf-depth-2">Student of CSE</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/notice" class="sf-depth-2">Notice</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/weekly" class="sf-depth-2">Weekly Test Result</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/programs" class="sf-depth-2">Course File</a></li>
                     <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/staff" class="sf-depth-2">Staff of CSE</a></li>
                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/cse/attendance" class="sf-depth-2">Attendance Summary</a></li>
                       </ul></div>

                    </section>
                      </div>
                          </aside>

            <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;"><p><span
                        style="color: #0000ff;"><strong>Departmental Laboratories</strong></span></p>
                    <ol>

                        <li>Programming Problems</li>
                        <li>Object Oriented Programming</li>
                        <li>Data Structure</li>
                        <li>Systems Programming</li>
						<li>Introduction to Java Programming Language</li>
						<li>DataBase Management Systems</li>
						<li>Operating Systems</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

<jsp:include page="/jsp/footer.jsp"/>
