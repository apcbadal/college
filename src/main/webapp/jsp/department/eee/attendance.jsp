<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Attendance</h3></div>

            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <aside class="col-sm-3" role="complementary">
                                                          <div class="region region-sidebar-second">
                                                <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                        <h2 class="block-title"> In the Department of Electrical & Electronics Engg.</h2>

                                                  <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                                 <ul class="menu nav">
                                                <li class="first leaf  menu-mlid-1154"><a href="http://localhost/department/eee/about" class="sf-depth-2 active">About Electrical & Electronics Engg.</a></li>
                                                <li class="leaf menu-mlid-1121"><a href="http://localhost/department/eee/vision" class="sf-depth-2">Vision & Mission</a></li>
                                                <li class="leaf menu-mlid-1156"><a href="http://localhost/department/eee/faculty" class="sf-depth-2">Faculty of Electrical & Electronics Engg.</a></li>
                                                <li class="leaf menu-mlid-1294"><a href="http://localhost/department/eee/labs" class="sf-depth-2">Labs & Infrastructure</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/question" class="sf-depth-2">Question Bank</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/awards" class="sf-depth-2">awards & Recognition</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/programs" class="sf-depth-2">Program Activities</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/students" class="sf-depth-2">Student of  Electrical & Electronics Engg.</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/notice" class="sf-depth-2">Notice</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/weekly" class="sf-depth-2">Weekly Test Result</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/programs" class="sf-depth-2">Course File</a></li>
                                                 <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/staff" class="sf-depth-2">Staff of  Electrical & Electronics Engg.</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/attendance" class="sf-depth-2">Attendance Summary</a></li>
                                                   </ul></div>

                                                </section>
                                                  </div>
                                                      </aside>
            <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;"><p>Updated Soon&#8230;</p>
                </div>
            </div>
        </div>
    </div>

<jsp:include page="/jsp/footer.jsp"/>
