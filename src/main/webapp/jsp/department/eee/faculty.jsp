<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Faculty of EEE</h3></div>

            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <aside class="col-sm-3" role="complementary">
                                                          <div class="region region-sidebar-second">
                                                <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                        <h2 class="block-title"> In the Department of Electrical & Electronics Engg.</h2>

                                                  <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                                 <ul class="menu nav">
                                                <li class="first leaf  menu-mlid-1154"><a href="http://localhost/department/eee/about" class="sf-depth-2 active">About Electrical & Electronics Engg.</a></li>
                                                <li class="leaf menu-mlid-1121"><a href="http://localhost/department/eee/vision" class="sf-depth-2">Vision & Mission</a></li>
                                                <li class="leaf menu-mlid-1156"><a href="http://localhost/department/eee/faculty" class="sf-depth-2">Faculty of Electrical & Electronics Engg.</a></li>
                                                <li class="leaf menu-mlid-1294"><a href="http://localhost/department/eee/labs" class="sf-depth-2">Labs & Infrastructure</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/question" class="sf-depth-2">Question Bank</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/awards" class="sf-depth-2">awards & Recognition</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/programs" class="sf-depth-2">Program Activities</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/students" class="sf-depth-2">Student of  Electrical & Electronics Engg.</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/notice" class="sf-depth-2">Notice</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/weekly" class="sf-depth-2">Weekly Test Result</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/programs" class="sf-depth-2">Course File</a></li>
                                                 <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/staff" class="sf-depth-2">Staff of  Electrical & Electronics Engg.</a></li>
                                                <li class="leaf menu-mlid-1467"><a href="http://localhost/department/eee/attendance" class="sf-depth-2">Attendance Summary</a></li>
                                                   </ul></div>

                                                </section>
                                                  </div>
                                                      </aside>
            <div class="col-md-8">
                <div class="page-content"><h2>Faculty Members</h2>
                    <hr/>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Prof. Ranjana Kumari&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<strong>(Principal)</strong>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/11/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/eee/principal1.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Electrical & Electronics Engg<br/> <strong>Designation:</strong>Associate Professor
                                    <br/> <strong>Phone Number:</strong>9576977097<br/> <strong>Email Address:</strong>
                                    <a href="mailto:principalranjanakri84@gmail.com" class="__cf_email__"
                                    principalranjanakri84@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/eee/principal"
                                                                          class="btn btn-default btn-lg">
                                    <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Ram Kumar</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/12/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/eee/rk.jpg)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Electrical & Electronics Engg<br/> <strong>Designation:</strong> Assistant Professor
                                    <br/> <strong>Phone Number:</strong> 9508527210<br/> <strong>Email Address:</strong>
                                    <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="422f2d2a2b3629372f2330022f2b362f373823242423303237306c2d3025">ramkumar.purnea@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/eee/rk" class="btn btn-default btn-lg">
                                    <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Amit Kumar Suman&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<strong>(Head
                            of the Department)</strong>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/13/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/eee/aks.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Electrical and Electronics Engineering<br/> <strong>Designation:</strong> Assistant
                                    Professor <br/> <strong>Phone Number:</strong>9835938174<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                             data-cfemail="d3a6b5b2a1bcbca2a6b6fdbebaa793b4beb2babffdb0bcbe">amit.suman2222@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/eee/aks"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Abhijeet Kumar</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/14/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/eee/ak.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Electrical and Electronics <br/> <strong>Designation:</strong> Assistant Professor
                                    <br/> <strong>Phone Number:</strong>8802927260<br/> <strong>Email Address:</strong>
                                    <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="8efde9fdefe5ebfabfcee9e3efe7e2a0ede1e3">abhijeet.mnit@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/eee/ak" class="btn btn-default btn-lg">
                                    <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Alauddin Ansari</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/15/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/eee/aa.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Electrical and Electronics<br/> <strong>Designation:</strong> Assistant Professor
                                    <br/> <strong>Phone Number:</strong>9027262492<br/> <strong>Email Address:</strong>
                                    <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="f9928c94988bd78b98929c8a91cbc0c0b99e94989095d79a9694">alauddinansari0432@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/eee/aa" class="btn btn-default btn-lg">
                                    <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Sajjad Ahmad</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/16/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/eee/sa.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Electrical and Electronics<br/> <strong>Designation:</strong> Assistant Professor
                                    <br/> <strong>Phone Number:</strong>8130952264<br/> <strong>Email Address:</strong>
                                    <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="5c2f343d383d3e2e3d3e3e3d32356e6c6c641c3b313d3530723f3331">sajjad06428@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/eee/sa" class="btn btn-default btn-lg">
                                    <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Saket Saurabh</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/17/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/eee/ss.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Electrical and Electronics<br/> <strong>Designation:</strong> Assistant Professor
                                    <br/> <strong>Phone Number:</strong>7004712309<br/> <strong>Email Address:</strong>
                                    <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                       data-cfemail="ef9f8e988e81849a828e9dc1858e869c988e83af88828e8683c18c8082">saket.srb@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/eee/ss" class="btn btn-default btn-lg">
                                    <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Tarique Rashid</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/18/image${fileExtension}"
                                                                             style="background-image: url(http://localhost/department/eee/tr.JPG)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Electrical and Electronics<br/> <strong>Designation:</strong> Assistant
                                    Professor<br/> <strong>Phone Number:</strong>9135826736<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                             data-cfemail="f4959e958ddac6ccda9f81999586b49399959d98da979b99">tariquerashid@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/eee/tr" class="btn btn-default btn-lg">
                                    <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading faculty-title"> Rajeev Dev Ranjan</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/19/image${fileExtension}"
                                                                                 style="background-image: url(http://localhost/department/eee/dummy.png)"/>
                                        <div class="visible-xs"><br/></div>
                                    </div>
                                    <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                        Electrical and Electronics<br/> <strong>Designation:</strong> Assistant
                                        Professor<br/> <strong>Phone Number:</strong>9835051301<br/> <strong>Email
                                            Address:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                                 data-cfemail="f4959e958ddac6ccda9f81999586b49399959d98da979b99">rajeevdevranjan@gmail.com</a>
                                        <div class="visible-xs"><br/></div>
                                    </div>
                                    <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/eee/rdr"
                                                                              class="btn btn-default btn-lg">
                                        <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                        Profile Page </a></div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading faculty-title">Naveen Kumar</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/20/image${fileExtension}"
                                                                                     style="background-image: url(http://localhost/department/eee/dummy.png)"/>
                                            <div class="visible-xs"><br/></div>
                                        </div>
                                        <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department
                                            of
                                            Electrical and Electronics<br/> <strong>Designation:</strong> Lab Assistant
                                            <br/> <strong>Phone Number:</strong>7870745188<br/> <strong>Email
                                                Address:</strong> <a href="/cdn-cgi/l/email-protection"
                                                                     class="__cf_email__"
                                                                     data-cfemail="f4959e958ddac6ccda9f81999586b49399959d98da979b99">naveeneesit@gmail.com</a>
                                            <div class="visible-xs"><br/></div>
                                        </div>
                                        <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/eee/nk"
                                                                                  class="btn btn-default btn-lg">
                                            <br class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br
                                                class="hidden-xs"/>
                                            Profile Page </a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></div></div>


<jsp:include page="/jsp/footer.jsp"/>
