<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Question Bank</h3></div>

            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
           <aside class="col-sm-3" role="complementary">
                                                        <div class="region region-sidebar-second">
                                              <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                      <h2 class="block-title">In the Department of Applied Sc. & Humanities</h2>

                                                <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                               <ul class="menu nav">
                                              <li class="first leaf  menu-mlid-1154"><a href="http://localhost/department/ash/about" class="sf-depth-2 active">About Applied Sc. & Humanities</a></li>
                                              <li class="leaf menu-mlid-1121"><a href="http://localhost/department/ash/vision" class="sf-depth-2">Vision & Mission</a></li>
                                              <li class="leaf menu-mlid-1156"><a href="http://localhost/department/ash/faculty" class="sf-depth-2">Faculty of Applied Sc. & Humanities</a></li>
                                              <li class="leaf menu-mlid-1294"><a href="http://localhost/department/ash/labs" class="sf-depth-2">Labs & Infrastructure</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/question" class="sf-depth-2">Question Bank</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/awards" class="sf-depth-2">Awards & Recognition</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/programs" class="sf-depth-2">Program Activities</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/students" class="sf-depth-2">Student of Applied Sc. & Humanities</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/notice" class="sf-depth-2">Notice</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/weekly" class="sf-depth-2">Weekly Test Result</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/programs" class="sf-depth-2">Course File</a></li>
                                               <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/staff" class="sf-depth-2">Staff of Applied Sc. & Humanities</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/attendance" class="sf-depth-2">Attendance Summary</a></li>
                                                 </ul></div>

                                              </section>
                                                </div>
                                                    </aside><div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                    <div id="pl-330" class="panel-layout">
                        <div id="pg-330-0" class="panel-grid panel-no-style">
                            <div id="pgc-330-0-0" class="panel-grid-cell" data-weight="1">
                                <div id="panel-330-0-0-0"
                                     class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                     data-index="0"
                                     data-style="{&quot;background&quot;:&quot;#c0f2bf&quot;,&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;}">
                                    <div class="panel-widget-style panel-widget-style-for-330-0-0-0">
                                        <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                            <div class="siteorigin-widget-tinymce textwidget">
                                                <table style="font-family: Georgia, arial, Times New Roman;"
                                                       align="center">
                                                    <tbody>
                                                    <tr style="border-bottom: 1pt solid black;">
                                                        <td style="border-bottom: 1pt solid black;">
                                                            <strong>Semester</strong></td>
                                                        <td style="border-bottom: 1pt solid black; text-align: center;"
                                                            colspan="2"><strong>Subjects</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-bottom: 1pt solid black;" rowspan="3"
                                                            width="66"><p style="text-align: center;">
                                                            <strong>ALL</strong></p></td>
                                                        <td bgcolor="#FCFCFC" width="354"><a
                                                                href="http://www.akubihar.com/">QUESTION BANK</a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>&nbsp;</p></div>
            </div>
        </div>
    </div>
    </div>


<jsp:include page="/jsp/footer.jsp"/>

