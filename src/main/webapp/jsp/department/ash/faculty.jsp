
<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Faculty of Applied Sc. & Humanities</h3></div>

    </div>
    <div class="container">

        <div class="row">
           <aside class="col-sm-3" role="complementary">
                                                        <div class="region region-sidebar-second">
                                              <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                                      <h2 class="block-title">In the Department of Applied Sc. & Humanities</h2>

                                                <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                               <ul class="menu nav">
                                              <li class="first leaf  menu-mlid-1154"><a href="http://localhost/department/ash/about" class="sf-depth-2 active">About Applied Sc. & Humanities</a></li>
                                              <li class="leaf menu-mlid-1121"><a href="http://localhost/department/ash/vision" class="sf-depth-2">Vision & Mission</a></li>
                                              <li class="leaf menu-mlid-1156"><a href="http://localhost/department/ash/faculty" class="sf-depth-2">Faculty of Applied Sc. & Humanities</a></li>
                                              <li class="leaf menu-mlid-1294"><a href="http://localhost/department/ash/labs" class="sf-depth-2">Labs & Infrastructure</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/question" class="sf-depth-2">Question Bank</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/awards" class="sf-depth-2">Awards & Recognition</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/programs" class="sf-depth-2">Program Activities</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/students" class="sf-depth-2">Student of Applied Sc. & Humanities</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/notice" class="sf-depth-2">Notice</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/weekly" class="sf-depth-2">Weekly Test Result</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/programs" class="sf-depth-2">Course File</a></li>
                                               <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/staff" class="sf-depth-2">Staff of Applied Sc. & Humanities</a></li>
                                              <li class="leaf menu-mlid-1467"><a href="http://localhost/department/ash/attendance" class="sf-depth-2">Attendance Summary</a></li>
                                                 </ul></div>

                                              </section>
                                                </div>
                                                    </aside>
            <div class="col-md-8">
                <div class="page-content"><h2>Faculty Members</h2>
                    <hr/>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title"> Dr. Pramod Kumar&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<strong>(Head of the Department of Mathematics)</strong></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/33/image${fileExtension}"
                                                                             style="background-image: url(dummy.png)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Mathematics<br/> <strong>Designation:</strong> Assistant Professor<br/> <strong>Phone
                                        Number:</strong>9117430155<br/> <strong>Email Address:</strong> <a
                                            href="/cdn-cgi/l/email-protection">pramodbhoola@gmail.com</a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/ash/pk"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Dr Vipin Kumar</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt="" src="http://localhost/34/image${fileExtension}"
                                                                             style="background-image: url(dummy.png)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of Applied Science and Humanities(Chemistry)<br/> <strong>Designation:</strong> Assistant
                                    Professor<br/> <strong>Phone Number:</strong>9939212181<br/> <strong>Email
                                        Address:</strong> <a href="/cdn-cgi/l/email-protection">drvpnkmr@gmail.com</a>

										<div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/ash/vk"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading faculty-title">Dr. B.N.Mahto</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3 box-profile-image"><img alt=""
                                                                             src="http://localhost/35/image${fileExtension}"
                                                                             style="background-image: url(dummy.png)"/>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                    Applied Science and Humanities(Physics)<br/> <strong>Designation:</strong> Assistant
                                    Professor<br/> <strong>Phone Number:</strong>9117430155<br/> <strong>Email
                                        Address:</strong> <a
                                            href="/cdn-cgi/l/email-protection"></a>
                                    <div class="visible-xs"><br/></div>
                                </div>
                                <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/ash/bnm"
                                                                          class="btn btn-default btn-lg"> <br
                                        class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                    Profile Page </a></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading faculty-title">Sunil Kumar</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-3 box-profile-image"><img alt=""
                                                                                 src="http://localhost/36/image${fileExtension}"
                                                                                 style="background-image: url(snk.jpg)"/>
                                        <div class="visible-xs"><br/></div>
                                    </div>
                                    <div class="col-sm-6 box-profile-info"><strong>Department:</strong> Department of
                                        Applied Science and Humanities<br/> <strong>Designation:</strong> Assistant
                                        Professor<br/> <strong>Phone Number:</strong>9973584973 <br/> <strong>Email
                                            Address:</strong> <a
                                                href="mailto: aviaguy@gmail.com"> aviaguy@gmail.com</a>
                                        <div class="visible-xs"><br/></div>
                                    </div>
                                    <div class="col-sm-3 box-profile-link"><a href="http://localhost/department/ash/snk"
                                                                              class="btn btn-default btn-lg"> <br
                                            class="hidden-xs"/> <i class="fa fa-link"></i> Visit<br class="hidden-xs"/>
                                        Profile Page </a></div>
                                </div>
                            </div></div>
                    </div>

</div>
                </div>
            </div>
        </div>
    </div>

<jsp:include page="/jsp/footer.jsp"/>
