<jsp:include page="/jsp/header.jsp"/>

<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-8"><h3>Awards and Recognition</h3></div>
                <div class="col-md-2 col-sm-4" style="font-size: 24px; line-height: 40px;"><span
                        style="display: inline-block; line-height: 40px; vertical-align: middle;"><i
                        </i> </span> <a
                        href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2F#%2Fdepartment-of-civil-engineering%2Fabout-civil-engineering%2F"
                        target="_blank" style="font-size: 30px; color: #333; vertical-align: middle;"><i
                        </i></a> <a
                        href="https://twitter.com/home?status=About+Civil+Engg.+-+https%3A%2F%2F#%2Fdepartment-of-civil-engineering%2Fabout-civil-engineering%2F"
                        target="_blank" style="font-size: 30px; color: #333; vertical-align: middle;"><i
                        </i> </a></div>
            </div>
        </div>
    </div>
    <div class="container">
        <ul class="breadcrumb" style="background: none">
            <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Awards and Recognition</li>
        </ul>
        <div class="row">
            <aside class="col-sm-3" role="complementary">
                                              <div class="region region-sidebar-second">
                                    <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                            <h2 class="block-title">In the Department of Civil Engg.</h2>

                                      <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                     <ul class="menu nav">
                                    <li class="first leaf  menu-mlid-1154"><a href="http://localhost/department/civil/about" class="sf-depth-2 active">About Civil Engg.</a></li>
                                    <li class="leaf menu-mlid-1121"><a href="http://localhost/department/civil/vision" class="sf-depth-2">Vision & Mission</a></li>
                                    <li class="leaf menu-mlid-1156"><a href="http://localhost/faculty?deptno=2" class="sf-depth-2">Faculty of Civil Engg.</a></li>
                                    <li class="leaf menu-mlid-1294"><a href="http://localhost/department/civil/labs" class="sf-depth-2">Labs & Infrastructure</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/question" class="sf-depth-2">Question Bank</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/awards" class="sf-depth-2">awards & Recognition</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/programs" class="sf-depth-2">Program Activities</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/students" class="sf-depth-2">Student of Civil Engg.</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/notice" class="sf-depth-2">Notice</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/weekly" class="sf-depth-2">Weekly Test Result</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/programs" class="sf-depth-2">Course File</a></li>
                                     <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/staff" class="sf-depth-2">Staff of Civil Engg.</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/attendance" class="sf-depth-2">Attendance Summary</a></li>
                                       </ul></div>

                                    </section>
                                      </div>
                                          </aside>
            <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td colspan="2">
                                <div class="colr1"><strong> Updated Soon &#8230<br/> </strong></div>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<jsp:include page="/jsp/footer.jsp"/>
