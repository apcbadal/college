<jsp:include page="/jsp/header.jsp"/>
<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">
        <div class="container">
            <div class="row">

            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
           <aside class="col-sm-3" role="complementary">
                                             <div class="region region-sidebar-second">
                                   <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                           <h2 class="block-title">In the Department of Civil Engg.</h2>

                                     <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                    <ul class="menu nav">
                                   <li class="first leaf  menu-mlid-1154"><a href="http://localhost/department/civil/about" class="sf-depth-2 active">About Civil Engg.</a></li>
                                   <li class="leaf menu-mlid-1121"><a href="http://localhost/department/civil/vision" class="sf-depth-2">Vision & Mission</a></li>
                               <li class="leaf menu-mlid-1156"><a href="http://localhost/faculty?deptno=2" class="sf-depth-2">Faculty of Civil Engg.</a></li>
                                   <li class="leaf menu-mlid-1294"><a href="http://localhost/department/civil/labs" class="sf-depth-2">Labs & Infrastructure</a></li>
                                   <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/question" class="sf-depth-2">Question Bank</a></li>
                                   <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/awards" class="sf-depth-2">awards & Recognition</a></li>
                                   <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/programs" class="sf-depth-2">Program Activities</a></li>
                                   <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/students" class="sf-depth-2">Student of Civil Engg.</a></li>
                                   <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/notice" class="sf-depth-2">Notice</a></li>
                                   <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/weekly" class="sf-depth-2">Weekly Test Result</a></li>
                                   <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/programs" class="sf-depth-2">Course File</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/staff" class="sf-depth-2">Staff of Civil Engg.</a></li>
                                   <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/attendance" class="sf-depth-2">Attendance Summary</a></li>
                                      </ul></div>

                                   </section>
                                     </div>
                                         </aside>
            <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                    <div id="pl-482" class="panel-layout">
                        <div id="pg-482-0" class="panel-grid panel-no-style">
                            <div id="pgc-482-0-0" class="panel-grid-cell" data-weight="1">
                                <div id="panel-482-0-0-0"
                                     class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                     data-index="0"
                                     data-style="{&quot;background&quot;:&quot;#a5d689&quot;,&quot;background_image_attachment&quot;:false,&quot;background_display&quot;:&quot;tile&quot;,&quot;font_color&quot;:&quot;#000000&quot;,&quot;link_color&quot;:&quot;#8224e3&quot;}">
                                    <div class="panel-widget-style panel-widget-style-for-482-0-0-0">
                                        <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                            <div class="siteorigin-widget-tinymce textwidget"><p
                                                    style="text-align: center;"><span
                                                    style="text-decoration: underline; font-family: georgia, palatino; font-size: large; color: #800000;"><b>Vision of the Department</b></span>
                                            </p>
                                                <p style="padding-left: 30px; text-align: justify;"><span
                                                        style="font-family: Georgia, Palatino;">To produce skilled civil engineers by providing quality technical education to fulfill the global needs.</span>
                                                </p>
                                                <hr/>
                                                <p style="text-align: center;"><span
                                                        style="text-decoration: underline; font-family: georgia, palatino; font-size: large; color: #800000;"><b>Mission of the Department</b></span>
                                                </p>
                                                <ul>
                                                    <li style="text-align: left;"><span
                                                            style="font-family: Georgia, Palatino; font-size: 12pt;">	To enhance the domain knowledge and employability of the student through quality education.</span>
                                                    </li>
                                                    <li style="text-align: left;"><span
                                                            style="font-family: Georgia, Palatino; font-size: 12pt;">	To induct research culture by application of modern tools and techniques.</span>
                                                    </li>
                                                    <li style="text-align: left;"><span
                                                            style="font-family: Georgia, Palatino; font-size: 12pt;">	To develop professional, social and ethical skills among the students.<span
                                                            style="font-family: georgia, palatino; font-size: medium;">.</span></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<jsp:include page="/jsp/header.jsp"/>
