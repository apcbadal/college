<jsp:include page="/jsp/header.jsp"/>


<div class="jumbotron" style="padding-top: 24px; padding-bottom: 24px;">

    <div class="container">
        <ul class="breadcrumb" style="background: none">
            <
        </ul>
        <div class="row">
            <aside class="col-sm-3" role="complementary">
                                              <div class="region region-sidebar-second">
                                    <section id="block-menu-block-3" class="block block-menu-block clearfix">

                                            <h2 class="block-title">In the Department of Civil Engg.</h2>

                                      <div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-2">
                                     <ul class="menu nav">
                                    <li class="first leaf  menu-mlid-1154"><a href="http://localhost/department/civil/about" class="sf-depth-2 active">About Civil Engg.</a></li>
                                    <li class="leaf menu-mlid-1121"><a href="http://localhost/department/civil/vision" class="sf-depth-2">Vision & Mission</a></li>
                              <li class="leaf menu-mlid-1156"><a href="http://localhost/faculty?deptno=2" class="sf-depth-2">Faculty of Civil Engg.</a></li>
                                    <li class="leaf menu-mlid-1294"><a href="http://localhost/department/civil/labs" class="sf-depth-2">Labs & Infrastructure</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/question" class="sf-depth-2">Question Bank</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/awards" class="sf-depth-2">awards & Recognition</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/programs" class="sf-depth-2">Program Activities</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/students" class="sf-depth-2">Student of Civil Engg.</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/notice" class="sf-depth-2">Notice</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/weekly" class="sf-depth-2">Weekly Test Result</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/programs" class="sf-depth-2">Course File</a></li>
                                     <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/staff" class="sf-depth-2">Staff of Civil Engg.</a></li>
                                    <li class="leaf menu-mlid-1467"><a href="http://localhost/department/civil/attendance" class="sf-depth-2">Attendance Summary</a></li>
                                       </ul></div>

                                    </section>
                                      </div>
                                          </aside>
            <div class="col-md-8">
                <div class="page-content" style="line-height: 175%; font-size: 125%; font-weight: 300;">
                    <div data-animation="bounceInUp" data-animation-delay="0s"><strong>The department has the following laboratories which provide practical instruction to undergraduate students </strong></p>
                        <ul>

                            <li><strong><span style="">Surveying Laboratory.</span></strong></li>
                            <ul><strong>List of Major Equipments :-</strong>
                                <li><span style="color:#122ae1;">Chain Surveying</span></li>
                                <li><span style="color:#122ae1;">Plane Table</span></li>
                                <li><span style="color:#122ae1;">Prismatic & Surveyor Compass</span></li>
                                <li><span style="color:#122ae1;">Dumpy level</span></li>
                                <li><span style="color:#122ae1;">Auto level</span></li>
                                <li><span style="color:#122ae1;">Theodolite</span></li>
                                <li><span style="color:#122ae1;">Total Station</span></li>
                            </ul>
                            <li><strong><span style="">Cement-Concrete Laboratory.</span></strong></li>
                            <ul><strong>List of Major Equipments:-</strong>
                                <li><span style="color:#122ae1;">Vicat's Apparatus</span></li>
                                <li><span style="color:#122ae1;">Le-chatelier Apparatus</span></li>
                                <li><span style="color:#122ae1;">Slump Cone</span></li>
                                <li><span style="color:#122ae1;">Needle Vibrator</span></li></ul>
                            <li><strong><span style="">Geo-Technical  Laboratory.</span></strong></li>
                            <ul><strong>List of Major Equipments:- </strong>
                                <li><span style="color:#122ae1;">Electric Oven</span></li>
                                <li><span style="color:#122ae1;">Sieve Shaker and sieve size </span></li></ul>
                            <li><strong><span style="">	Environmental Engineering Laboratory.</span></strong>(Updated Soon)</li>
                            <li><span
                                    style=""><strong>Highway Engineering Laboratory </strong>(Updated Soon)</span>
                            </li>
                            <li><strong><span style="">	Material Testing Laboratory</span></strong>(Updated Soon)</li>
                            <li><strong><span style="">	Hydraulics Laboratory</span></strong>(Updated Soon)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<jsp:include page="/jsp/footer.jsp"/>
