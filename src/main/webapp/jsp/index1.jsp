<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"><link rel="shortcut icon" href="http://localhost/favicon.png" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en" />
<link rel="icon" type="image/png" href="favicon.png" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<script data-cfasync="false" id="ao_optimized_gfonts" type="text/javascript">WebFontConfig={google:{families:['Lato:300,400'] },classes:false, events:false, timeout:1500};(function() {var wf = document.createElement('script');wf.src='https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';wf.type='text/javascript';wf.async='true';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(wf, s);})();




</script>
<link type="text/css" media="all" href="wp-content/cache/autoptimize/css/autoptimize_e5f671b1828a9e81941de090a06f0ed0.css" rel="stylesheet" />
<title>
Katihar Engineering College</title>
<link href="wp-content/themes/kec/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="wp-content/themes/kec/assets/vendors/bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet" />
<link href="wp-content/themes/kec/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<link href="wp-content/themes/kec/assets/css/editor.css" rel="stylesheet" />
<link href="wp-content/themes/kec/assets/css/common.css" rel="stylesheet" />
<link id="theme" href="wp-content/themes/kec/assets/css/default.css" rel="stylesheet" />
<link href="wp-content/themes/kec/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />
 <link href="../wp-content/themes/kec/assets/css/style.css" rel="stylesheet"/>

 <!--[if lt IE 9]>
 <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
 <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <![endif]-->
<link href='https://fonts.gstatic.com' crossorigin='anonymous' rel='preconnect' />
<link href='https://ajax.googleapis.com' rel='preconnect' />
<link href='https://fonts.googleapis.com' rel='preconnect' />
<link rel='stylesheet' id='uaf_client_css-css'wp-content/uploads/useanyfont/uaf.css' type='text/css' media='all' />
<link rel='https://api.w.org/' href='https://#/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://#/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://#/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.9.8" />
<link rel="alternate" type="application/json+oembed" href="https://#/wp-json/oembed/1.0/embed?url=https%3A%2F%2F#%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://#/wp-json/oembed/1.0/embed?url=https%3A%2F%2F#%2F&#038;format=xml" />
<link rel="stylesheet" href="wp-content/themes/kec/assets/vendors/nivo-lightbox/dist/nivo-lightbox.min.css" />
<link rel="stylesheet" href="wp-content/themes/kec/assets/vendors/nivo-lightbox/themes/default/default.css" />
<script src="wp-content/themes/kec/assets/vendors/jquery.cookie/jquery.cookie.js></script>
<script src="wp-content/themes/kec/assets/vendors/jquery/dist/jquery.min.js></script>
<script src="wp-content/themes/kec/assets/vendors/bootstrap-sweetalert/dist/sweetalert.js></script>
<script src="wp-content/themes/kec/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="wp-content/themes/kec/assets/vendors/waypoints/lib/jquery.waypoints.min.js></scripts>
<script src="wp-content/themes/kec/assets/vendors/waypoints/lib/shortcuts/sticky.min.js></script>
<script src="wp-content/themes/kec/assets/vendors/nivo-lightbox/dist/nivo-lightbox.min.js></script>
</head>

<body class="home page-template-default page page-id-2"><div id="fb-root"></div> <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
 <header>
<div class="top-header">
<div class="container" style="position: relative;">
<ul style="list-style-type: none; position: absolute; top: -1px; right: 10px; z-index: 50;">
<li style="display: inline-block; vertical-align: top; margin-top: 5px;">Color Scheme:</li>
<li style="display: inline-block; padding: 1px;">
<span onclick="changeTheme('default.css')" style="display: inline-block; width: 25px; height: 25px; cursor: pointer; background-color: #3253ca;"></span></li>
<li style="display: inline-block; padding: 1px;">
<span onclick="changeTheme('theme1.css')" style="display: inline-block; width: 25px; height: 25px; cursor: pointer; background-color: #6E1A98;"></span></li>
<li style="display: inline-block; padding: 1px;">
<span onclick="changeTheme('theme2.css')" style="display: inline-block; width: 25px; height: 25px; cursor: pointer; background-color: #F0C463;"></span></li>
</ul>
<div class="visible-xs"><br /><br /></div>
<div class="row">

<div class="col-sm-7"><div class="logo-left pull-left"><img alt="" src="logo1.jpeg"class="img-responsive" style="height: 100px; margin-right: 5px;"></div><a href="http://www.localhost" class="logo-right pull-left"> <span style="font-size: 70%;">Katihar Engineering College, Katihar</span><br> <span style="font-size: 30%; margin-top: -10px;" class="pull-left">( Dept. Of Science &amp; Technology, Govt. Of Bihar )</span> </a><div class="clearfix">
</div>
</div>
<div class="col-sm-5 text-right">
<div class="top-header-menu-wrapper">
<ul class="top-header-menu">
<li><a href="/">Home</a></li><li><a href="#modal-inquiry-form" data-toggle="modal">Inquiry Form</a></li>
<li><a href="about-kec/search-student.html"><i class="fa fa-search"></i> &nbsp;Search Student</a></li>
<!--<li><a href="https://#/teqip-iii/about-teqip-iii/" target="_blank">TEQIP</a></li>-->
<li><a href="approval-kec/aicte-approval.html" target="_blank">AICTE</a></li>
<li><a href="category/kec-web-team.html" target="_blank">KEC Web Team</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<nav class="navbar navbar-default">
<div class="container">
<div class="navbar-header">
 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
 <span class="icon-bar"></span>
<span class="icon-bar"></span>
 </button>
</div>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li class="menubar-logo hidden-xs">

</a>
</li>

<li><a style="font-weight: 600;" href="/">Home</a></li>
<li class="dropdown">
<a style="font-weight: 600;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About KEC <span class="fa fa-angle-down"></span></a>
<ul class="dropdown-menu">
<li><a href="about-kec/history.html">History</a></li>
<li><a href="about-kec/principal-message.html">Principal's Message</a></li>
<li><a href="about-kec/administration.html">Administration</a></li>
<li><a href="about-kec/vision.html">Vision & Mission</a></li>
<li><a href="about-kec/affiliation.html">Affiliation</a></li>
<li><a href="about-kec/allotment-surrender.html">Allotment and Surrender Report</a></li>
<li><a href="about-kec/contact-us.html">Contact Us</a></li></ul></li>
<li class="dropdown"> <a style="font-weight: 600;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Academic <span class="fa fa-angle-down"></span></a>
<ul class="dropdown-menu">
<li>
<a href="academic-kec/ug-admission.html">UG Admission</a></li>
<li><a href="academic-kec/academic-regulations.html">Academic Regulations</a></li>
<li><a href="academic-kec/academic-calender.html">Academic Calender</a></li>
<li><a href="academic-kec/list-of-holidays.html">List of Holidays</a></li>
<li><a href="academic-kec/attendance.html">Attendance</a></li>
<li><a href="academic-kec/syllabus.html">Syllabus</a></li>
<li><a href="academic-kec/disciplinary-rules.html">Disciplinary Rules</a></li>
<li><a href="academic-kec/anti-ragging.html">Anti Ragging</a></li>
<li><a href="academic-kec/mom-of-academic-council.html">MoM of Academic Council</a></li>
<li><a href="academic-kec/notice-from-govt.html">Notice from Govt.</a></li>
</ul>
</li>
<li class="dropdown">
<a style="font-weight: 600;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Department <span class="fa fa-angle-down"></span></a>
<ul class="dropdown-menu">
<li><a href="department-kec/civil-about.html">Civil Engineering</a></li>
<li><a href="department-kec/mech-about.html">Mechanical Engineering</a></li>
<li><a href="department-kec/eee-about.html">Electrical and Electronics Engg.</a></li>
<li><a href="department-kec/cse-about.html">Computer Sc.. & Engineering</a></li>
<li><a href="department-kec/ash-about.html">Applied Sc. & Humanities</a></li>
</ul>
</li>
<li class="dropdown">
 <a style="font-weight: 600;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Facilities & Services <span class="fa fa-angle-down"></span></a>
<ul class="dropdown-menu">
<li>
<a href="training-placement-kec/placement-brochure.html">Placement</a></li>
<li><a href="facilities-services-kec/cse-center.html">Computer Centre</a></li>
<li><a href="facilities-services-kec/startup-cell.html">Startup Cell</a></li>
<li><a href="facilities-services-kec/centtral-library.html">Central Library</a></li>
<li><a href="facilities-services-kec/useful-link.html">Useful-Link</a></li>
<li><a href="facilities-services-kec/hostels.html">Hostels</a></li>
<li><a href="facilities-services-kec/sports-facilities.html">Sports Facilities</a></li>
<li><a href="facilities-services-kec/medical-facilities.html">Medical Facilities</a></li>
<li><a href="facilities-services-kec/bank.html">Bank</a></li>
<li><a href="facilities-services-kec/guest-house.html">Guest House</a></li>
<li><a href="facilities-services-kec/club.html">Club</a></li>
<li><a href="facilities-services-kec/gymnasium.html">Gymnasium</a></li>
<li><a href="facilities-services-kec/health-center.html">Health Center</a></li>
<li><a href="facilities-services-kec/wi-fi.html">Wi Fi</a></li>
</ul>
</li>
<li class="dropdown">
 <a style="font-weight: 600;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Training & Placement <span class="fa fa-angle-down"></span></a>
<ul class="dropdown-menu">
<li><a href="training-placement-kec/about-placement.html">About Placement</a></li>
<li><a href="training-placement-kec/placement-brochure.html">Placement Brochure</a></li>
<li><a href="training-placement-kec/rules-of-training-placement.html">Rules of Training &#038; Placement</a></li>
<li><a href="training-placement-kec/tips-for-resume.html">Tips for Resume</a></li>
<li><a href="training-placement-kec/student-placement-coordinator.html">Students Placement Coordinator</a></li>
</ul>
</li>
<li class="dropdown"> <a style="font-weight: 600;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gallery <span class="fa fa-angle-down"></span></a>
<ul class="dropdown-menu">
<li><a href="gallery-kec/media-gallery.html">Media Gallery</a></li>
<li><a href="gallery-kec/photo-gallery.html">Photo Gallery</a></li>
<li><a href="gallery-kec/video-gallery.html/">Video Gallery</a></li>
</ul>
</li>
<li class="dropdown">
<a style="font-weight: 600;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Approval <span class="fa fa-angle-down"></span></a>
<ul class="dropdown-menu">
<li><a href="approval-kec/aicte-approval.html">AICTE Approval</a></li>

<li><a href="approval-kec/aku-approval.html">AKU Approval</a></li>
</ul>
</li>
<li class="dropdown">
 <a style="font-weight: 600;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">RTI <span class="fa fa-angle-down"></span></a>
<ul class="dropdown-menu">
<li><a href="rti-kec/rti-act.html">RTI act 2005</a></li>
<li><a href="rti-kec/rti-application.html">RTI Application Form</a></li>
<li><a href="rti-kec/rti-status.html">RTI Status</a></li>
<li><a href="rti-kec/public-information-cell.html">Public Information Cell</a></li>
</ul>
</li>
<li class="dropdown">
<a style="font-weight: 600;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login <span class="fa fa-angle-down"></span></a>
<ul class="dropdown-menu">

<li><a href="/login">Admin</a></li>
<li><a href="/logout">Logout</a></li>

</div>
</div>
</nav>
</header>
<div class="modal fade" id="modal-inquiry-form">
<div class="modal-dialog">
<form method="post" onsubmit="return doInquiry(this)">
<input type="hidden" name="action" value="ajaxhandler" />
 <input type="hidden" name="xaction" value="postInquiry" />
<div class="modal-content">
<div class="modal-header">
<button class="close" type="button" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Inquiry Form</h4><p>If you have any questions, please do ask us by filling the form below.</p></div>
<div class="modal-body">
<div class="form-group">
<label class="control-label">Your Name:</label> <input type="text" name="fullName" class="form-control" required /></div>
<div class="form-group">
 <label class="control-label">Phone Number:</label>
<input type="tel" name="phoneNumber" class="form-control" required />
</div>
<div class="form-group">
<label class="control-label">Email Address:</label>
<input type="email" name="emailAddress" class="form-control" /></div>
<div class="form-group">
 <label class="control-label">Message:</label>
<textarea name="message" class="form-control" style="height: 150px;" required placeholder="Please enter your query here..."></textarea>
</div>
</div>
<div class="modal-footer">
<button type="submit" data-loading-text="<i class='fa fa-cog fa-spin'></i> Please wait..." class="btn btn-success"><i class="fa fa-check"></i> Submit</button> <button type="button" data-dismiss="modal" class="btn btn-default"><i class="fa fa-times"></i> Cancel</button></div></div></form></div></div>
<section id="heroBlock">
<div class="container-fluid">
<div class="row">
<div class="heroCol col-sm-8">
<div id="homePageSlider" class="carousel slide" data-ride="carousel" data-interval="8000">
<ol class="carousel-indicators">
<li data-target="#homePageSlider" data-slide-to="0" class="active"></li>
<li data-target="#homePageSlider" data-slide-to="1"></li>
<li data-target="#homePageSlider" data-slide-to="2"></li>
<li data-target="#homePageSlider" data-slide-to="3"></li>
<li data-target="#homePageSlider" data-slide-to="4"></li>
<li data-target="#homePageSlider" data-slide-to="5"></li>
</ol>
<div class="carousel-inner" role="listbox">
<div class="item active">
<div class="slideImage" style="background-position: center; background-size: cover; background-image: url(wp-content/uploads/2018/img12.JPG)">
<div class="slideInfo " data-animation="fadeInUp"><p><em> <strong>Library</strong></em></em> </p></div></div>
</div>
a
<div class="item ">
<div class="slideImage" style="background-position: center; background-size: cover; background-image: url(wp-content/uploads/2018/img3.JPG)">
<div class="slideInfo " data-animation="fadeInUp">Students converging the aspects of information technology in <strong>Computer Lab</strong><p><em></em>  <em></em> <em></em> </p></div></div></div>
<div class="item ">
<div class="slideImage" style="background-position: center; background-size: cover; background-image: url(wp-content/uploads/2018/img10.JPG)">
<div class="slideInfo " data-animation="fadeInUp"><P><strong>AAGAZ-2018:</strong>The cultural event to broaden the horizons and gain unique insights into our own communities as well as a broader global perspective of our diverse cultures.</p><div class="text-left"> </div></div></div></div>
<div class="item ">
<div class="slideImage" style="background-position: center; background-size: cover; background-image: url(wp-content/uploads/2018/img11.jpg)">
<div class="slideInfo " data-animation="fadeInUp">New Building &nbsp(Under Construction)<div class="text-left"> <a href="gallery-kec/photo-gallery.html" class="btn btnSlideInfo">More</a></div></div></div></div></div>
 <a class="left carousel-control" href="#homePageSlider" role="button" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
 <span class="sr-only">Previous</span> </a>
<a class="right carousel-control" href="#homePageSlider" role="button" data-slide="next">
<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
 <span class="sr-only">Next</span>
</a>
</div>
</div>
<div class="heroCol col-sm-4">
<div class="panel panel-default">
<div class="panel-heading">
<strong>
 <i class="fa fa-news"></i> Latest News / Updates</strong>
 <c:if test = "${Role == 'Admin'}">
     <strong><i class="fa fa-news"></i> <a href="/auth/uploadfile/upload">Upload Notice / News</a></strong>
     <strong><i class="fa fa-news"></i> <a href="/auth/sendMessage">Send Message</a></strong>
 </c:if></div>
<div class="panel-body" style="max-height: 500px;">
 <ul class="listUpdates" id="noticeBoardItems">

 <li style="display: block;">
  	<div>
  		<span class="label" style="border:  solid 1px #ccc; color: #000;">
 		<i class="fa fa-clock-o"></i> 2019-01-03 </span>&nbsp; <span class="label label-primary">News</span><a href="https://www.aicte-india.org/feedback/" target="_blank">Faculties and student may give feedback to A.I.C.T.E here</a>
  	</div>
  </li>

 <c:forEach items="${noticeList}" var="notice">
   <li>
 	<div>
 		<span class="label" style="border:  solid 1px #ccc; color: #000;">
 		<c:set var = "clazz"  value = "label label-warning"/>
 		<c:set var = "filePath"  value = "http://localhost/wp-content/uploads/notice/"/>
 		 <c:if test = "${ notice.noticeType == 'News'}">
                  <c:set var = "clazz"  value = "label label-primary"/>
          </c:if>
 		<i class="fa fa-clock-o"></i> ${notice.date} </span>&nbsp; <span class="${clazz}">${notice.noticeType}</span>
 		<c:if test = "${Role == 'Admin'}">
 		    <span class="label label-danger"><a href="<c:url value='/auth/${notice.uploadedFileName}/${notice.id}' />">Delete</a></span>
         </c:if>
 		<a href="${filePath}${notice.uploadedFileName}" target="_blank">${notice.headLine}</a>
 	</div>
 </li>
 </c:forEach>
 </ul>
 <a href="http://localhost/category/notices" class="btn btn-default">View All<i class="fa fa-double-angle-right"></i></a>
</div>
</div>
</div>
</div>
</div>
</section>
<jsp:include page="header.jsp"/>
<header role="banner" id="page-header">

    <div class="region region-header">
        <section id="block-views-nodequeue-2-block" class="block block-views clearfix">


            <div class="view view-nodequeue-2 view-id-nodequeue_2 view-display-id-block view-dom-id-adc75da57891ed7e5b0d70e0b788ead9">


                <div class="view-content">

                    <div class="skin-default">

                        <div id="views_slideshow_cycle_main_nodequeue_2-block_1"
                             class="views_slideshow_cycle_main views_slideshow_main">
                            <div id="views_slideshow_cycle_teaser_section_nodequeue_2-block_1"
                                 class="views-slideshow-cycle-main-frame views_slideshow_cycle_teaser_section">
                                <div id="views_slideshow_cycle_div_nodequeue_2-block_1_0"
                                     class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-1 views-row-first views-row-odd"
                                     aria-labelledby='views_slideshow_pager_field_item_bottom_nodequeue_2-block_1_0'>
                                    <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd views-row-first">

                                        <div class="views-field views-field-field-image">
                                            <div class="field-content"><img typeof="foaf:Image" class="img-responsive"
                                                                            src="http://localhost/sites/default/files/img12.JPG"
                                                                            width="2000" height="550" alt=""/></div>
                                        </div>
                                        <div class="views-field views-field-title"><span
                                                class="field-content">Library </span>
                                        </div>
                                    </div>
                                </div>


                            <div id="views_slideshow_controls_text_nodequeue_2-block_1"
                                 class="views-slideshow-controls-text views_slideshow_controls_text">
  <span id="views_slideshow_controls_text_previous_nodequeue_2-block_1"
        class="views-slideshow-controls-text-previous views_slideshow_controls_text_previous">
  <a href="#" rel="prev">Previous</a>
</span>
                                <span id="views_slideshow_controls_text_pause_nodequeue_2-block_1"
                                      class="views-slideshow-controls-text-pause views_slideshow_controls_text_pause  views-slideshow-controls-text-status-play"><a
                                        href="#">Pause</a></span>
                                <span id="views_slideshow_controls_text_next_nodequeue_2-block_1"
                                      class="views-slideshow-controls-text-next views_slideshow_controls_text_next">
  <a href="#" rel="next">Next</a>
</span>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </section>
    </div>
</header> <!-- /#page-header -->

<div class="gt-newsblocksection">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="region region-home-1">
                    <section id="block-block-18" class="block block-block clearfix">

                        <h2 class="block-title">Latest Notice</h2>

                        <div class="rwsvtickercontent">
                            <div class="rwsvticker1">
                                <ul>
                                    <li><span style="font-size:14px"><span
                                            style="font-family:arial,helvetica,sans-serif"><a
                                            href="https://www.aicte-india.org/feedback/" target="_blank">Student and Faculty give feedback to AICTE</a></span></span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="region region-home-2">
                    <section id="block-views-nodequeue-12-block" class="block block-views clearfix">

                        <h2 class="block-title">Tenders</h2>

                        <div class="view view-nodequeue-12 view-id-nodequeue_12 view-display-id-block view-dom-id-1584ef8e1537a413a20e095a49a0704e">


                            <div class="view-content">
                                <div class="item-list">
                                    <ul>
                                        <li class="views-row views-row-1 views-row-odd views-row-first">
                                            <div class="views-field views-field-title"><span class="field-content">No tenders.</span>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>


                            <div class="more-link">
                                <a href="/notices">
                                    More </a>
                            </div>


                        </div>
                    </section>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="region region-home-3">
                    <section id="block-block-11" class="block block-block clearfix">

                        <h2 class="block-title">Academics</h2>

                        <ul>
                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="http://akubihar.ac.in/Academics/Docs/NEW_CURRICULUM_ENGINEERING_TECHNOLOGY.pdf"
                                    target="_blank">New Course Curriculum for Engineering & Technology for 1st & 2nd Semester based on AICTE Model Cirriculum to be followed from Session 2018-19 onwards </a></span>
                            </li>
                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="http://akubihar.ac.in/Academics/Docs/revised%20B%20tech%2019-02-11(revised).pdf"
                                    target="_blank">Revised B.tech Syllabus</a></span></li>

                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="/newsletter">News Letter</a></span></li>
                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="academic-kec/calender.pdf">Holidays List</a></span></li>
                        </ul>

                    </section>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="region region-home-4">
                    <section id="block-block-12" class="block block-block clearfix">

                        <h2 class="block-title">Important Links</h2>

                        <ul>
                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="http://socialwelfare.bih.nic.in/"
                                    target="_blank">Social Welfare Dept. Bhar</a></span>
                            </li>
                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="http://dstbihar.gov.in/" target="_blank">DST Bihar</a></span></li>
                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="http://www.educationbihar.gov.in/login.aspx?ReturnUrl=%2fDefault.aspx">Education Dept. Bihar</a></span>
                            </li>
                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="http://gov.bih.nic.in/">Govt. of Bihar</a></span>
                            </li>
                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="http://bceceboard.bihar.gov.in/">BCECEB</a></span></li>
                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="http://akubihar.ac.in/">AKU, Patna</a></span></li>
                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="https://www.aicte-india.org/" target="_blank">AICTE</a></span>
                            </li>
                            <li><span style="font-family:arial,helvetica,sans-serif"><a
                                    href="https://jeemain.nic.in/webinfo/Public/Home.aspx">Joint Entrance Examination</a></span>
                            </li>
                            <!--	<li><a href="https://www.vidyalakshmi.co.in/Students/" target="_blank">Vidya Lakshmi Portal</a></li>-->
                        </ul>

                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="gtmaincontenthome">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <a id="main-content"></a>
                <h1 class="page-header">About KEC</h1>

                <div class="region region-content">
                    <section id="block-system-main" class="block block-system clearfix">


                        <article id="node-1" class="node node-page clearfix" about="/home" typeof="foaf:Document">
                            <header>
                                <span property="dc:title" content="About Us" class="rdf-meta element-hidden"></span>
                            </header>
                            <div class="field field-name-body field-type-text-with-summary field-label-hidden">
                                <div class="field-items">
                                    <div class="field-item even" property="content:encoded"><p><strong>Katihar
                                        Engineering College (KEC), Katihar </strong>with the advent of technology and
                                        the emergence of notable education in our society, had decided to adapt the
                                        prevailing need of light. In the mission to achieve the eminence in the field,
                                        the establishment of Katihar Engineering College was laid in the year 2016. The
                                        college is enriched with a pollyannaish environment, highly motivated and
                                        enthusiastic students guided by well qualified professional from different
                                        prestigious colleges of our country. The college was established with three
                                        branches- Civil Engineering, Mechanical Engineering and Computer Science and
                                        Engineering. Each branch has a number of renowned professors with high
                                        qualifications and experience in teaching or their respective industries. The
                                        college consists of a central library, Computer Labs, Training and Placement
                                        Cell, Programmer's Club etc, with the exceptional support staffs and
                                        infrastructure. All the labs and other infrastructure is in highly conditional
                                        states serving at their best to provide the best facilities to all the members
                                        of the college. Student of the college have exhibited their skill at different
                                        platform and gathered many awards from premium institutes. Presently the college
                                        functions in Government Polytechnic, Bheriya Rhekia, Katihar. It is situated 3
                                        km from Katihar Railway Station and there is a good transportation facility
                                        available from there. Currently, the own campus for the college is
                                        under-construction in Hajipur, Katihar near NH 31. It is expected to complete
                                        soon.<a
                                                href="about-kec/history.html">more</a>...</p>
                                    </div>
                                </div>
                            </div>
                        </article>

                    </section>
                </div>
            </div>
            <div class="col-sm-6">
                &nbsp;
            </div>
        </div>
    </div>
</div>


<div class="main-container container" style="display:none;">
    <div class="row">

        <section class="col-sm-12">

            <div class="alert alert-block alert-dismissible alert-danger messages error">
                <a class="close" data-dismiss="alert" href="#">&times;</a>
                <h4 class="element-invisible">Error message</h4>
                <em class="placeholder">Notice</em>: unserialize(): Error at offset 1090 of 9489 bytes in <em
                    class="placeholder">views_db_object-&gt;load_row()</em> (line <em class="placeholder">2265</em> of
                <em class="placeholder">/var/www/html/sites/all/modules/contrib/views/includes/view.inc</em>).
            </div>

        </section>


    </div>
</div>


<div class="gt-galleryvideosection">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="region region-home-gallery">
                    <section id="block-views-nodequeue-10-block" class="block block-views clearfix">

                        <h2 class="block-title">Gallery</h2>

                        <div class="view view-nodequeue-10 view-id-nodequeue_10 view-display-id-block view-dom-id-092f3c9d9a1a277eabf025598077161b">


                            <div class="view-content">
                                <div class="item-list">
                                    <ul>
                                        <li class="views-row views-row-1 views-row-odd views-row-first">
                                            <div class="views-field views-field-field-image">
                                                <div class="field-content"><a href="/pitch-better-india"><img
                                                        typeof="foaf:Image" class="img-responsive"
                                                        src="http://localhost/sites/default/files/styles/our_gallery_home_page/public/Pitch%20for%20a%20Better%20India_1.jpg?itok=U6XG7P1q"
                                                        width="330" height="225" alt=""
                                                        title="Pitch for a Better India"/></a></div>
                                            </div>
                                        </li>
                                        <li class="views-row views-row-2 views-row-even">
                                            <div class="views-field views-field-field-image">
                                                <div class="field-content"><a href="/ace-psychometric"><img
                                                        typeof="foaf:Image" class="img-responsive"
                                                        src="http://localhost/sites/default/files/styles/our_gallery_home_page/public/Ace%20the%20Psychometric_1.jpg?itok=4GiuxxhB"
                                                        width="330" height="225" alt=""
                                                        title="Ace the Psychometric"/></a></div>
                                            </div>
                                        </li>
                                        <li class="views-row views-row-3 views-row-odd views-row-last">
                                            <div class="views-field views-field-field-image">
                                                <div class="field-content"><a href="/workshop"><img typeof="foaf:Image"
                                                                                                    class="img-responsive"
                                                                                                    src="http://localhost/sites/default/files/styles/our_gallery_home_page/public/Disassembly%20Workshop_01.JPG?itok=29HGx6Gr"
                                                                                                    width="330"
                                                                                                    height="225" alt=""
                                                                                                    title="Machine Assembly and Disassembly Workshop"/></a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                            <div class="more-link">
                                <a href="gallery-kec/photo-gallery.jsp">
                                    View All </a>
                            </div>


                        </div>
                    </section>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="region region-home-video">
                    <section id="block-block-20" class="block block-block clearfix">

                        <h2 class="block-title">Video Gallery</h2>

                        <ul>
                            <li>
                                <div class="views-field views-field-field-image">
                                    <div class="field-content embed-responsive embed-responsive-16by9">
                                        <iframe frameborder="0" height="551"
                                                src="https://www.youtube.com/embed/DykZZgT9UHE" width="1050"></iframe>
                                    </div>
                                </div>
                                <div class="views-field views-field-title">Fresher's Party Event
                                </div>
                            </li>
                        </ul>
                        <div class="more-link"><a href="gallery-kec/video-gallery.jsp">View All </a></div>

                    </section>
                </div>
            </div>

        </div>
    </div>
</div>

<jsp:include page="footer.jsp"/>
<section id="openingSection">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="siteOpeningContent" data-animation="bounceInDown" data-animation-delay="1s" style="text-align: justify;"><p> <strong>Katihar Engineering College (KEC)</strong>, Katihar
                                                                                                                                                                              with the advent of technology and the emergence of notable education in our society, had decided to adapt the prevailing need of light. In the mission to achieve the eminence in the field, the establishment of Katihar Engineering College was laid in the year 2016. The college is enriched with a pollyannaish environment, highly motivated and enthusiastic students guided by well qualified professional from different prestigious colleges of our country. The college was established with three branches- Civil Engineering, Mechanical Engineering and Computer Science and Engineering. Each branch has a number of renowned professors with high qualifications and experience in teaching or their respective industries. The college consists of a central library, Computer Labs, Training and Placement Cell, Programmer's Club etc, with the exceptional support staffs and infrastructure. All the labs and other infrastructure is in highly conditional states serving at their best to provide the best facilities to all the members of the college. Student of the college have exhibited their skill at different platform and gathered many awards from premium institutes. Presently the college functions in Government Polytechnic, Bheriya Rhekia, Katihar. It is situated 3 km from Katihar Railway Station and there is a good transportation facility available from there. Currently, the own campus for the college is under-construction in Hajipur, Katihar near NH 31. It is expected to complete soon.

</div>
<div class="text-right">
 <a href="about-kec/history.html">Read More <i class="fa fa-angle-double-right"></i></a>
</div>
</div>
</div>
</div>
</section>
<section id="linkBoxWrapper">
<div class="container">
<ul id="linkBoxList">
<li data-animation="bounceInLeft" data-animation-delay="0ms">
<div class="linkBox">
 <a href="">
 <span class="content text-left" style="background-image: url(images.png)">
<span class="contentWrapper" data-hover-animation="slideInUp">The objective of this Skill Certification Scheme is to enable a large number of Indian youth to take up industry-relevant skill training that will help them in securing a better livelihood. Individuals with prior learning experience or skills will also be assessed and certified under Recognition of Prior Learning (RPL). Under this Scheme, Training and Assessment fees are completely paid by the Government.
</span>
</span>
<span class="caption"> Skill Development Mission <span class="pull-right">
<i class="fa fa-angle-right"></i>
</span>
 </span>
 </a>
</div>
</li>
<li data-animation="bounceInLeft" data-animation-delay="200ms">
<div class="linkBox">
 <a href="https://www.facebook.com/codingkec/?ref=page_internal">
 <span class="content text-left" style="background-image: url(wp-content/uploads/2018/coding.jpg)">
<span class="contentWrapper" data-hover-animation="slideInUp">Here is the announcement of the coding club of KEC. The goal is to nurture the basics of programming language.</span>
 </span>
 <span class="caption"> Coding Club<span class="pull-right">
<i class="fa fa-angle-right">
</i>
</span>
 </span>
 </a>
</div>
</li>
<!--<li data-animation="bounceInLeft" data-animation-delay="400ms">
<div class="linkBox">
<a href="https://mitaa.co.in">
<span class="content text-left" style="background-image: url(wp-content/uploads/2018/coding.jpg)">
 <span class="contentWrapper" data-hover-animation="slideInUp"> Student's Alumni Cell (SAC) KEC, is a student body that was formed with the aim to bridge the gap between the alumni and current students/University.It will also act as the Alma mater's representative for the alumni that will be available to help them out for any kind of academic/administrative work in the University. </span>
 </span>
 <span class="caption"> Alumni Affairs of mit <span class="pull-right"><i class="fa fa-angle-right"></i>
</span>
 </span>
 </a>
</div>
</li>-->
<li data-animation="bounceInLeft" data-animation-delay="600ms">
<div class="linkBox">
<a href="approval-kec/aicte-approval.html">
 <span class="content text-left" style="background-image: url(AICTE.png)">
 <span class="contentWrapper" data-hover-animation="slideInUp"> All India Council for Technical Education (AICTE) was set up in November 1945 as a national-level Apex Advisory Body for Technical education in India ..... </span>
 </span>
 <span class="caption"> AICTE BODY <span class="pull-right">
<i class="fa fa-angle-right">
</i>
</span>
 </span>
 </a>
</div>
</li>
<!--<li data-animation="bounceInLeft" data-animation-delay="800ms">
<div class="linkBox">
 <a h   ref="https://#/teqip-iii/about-teqip-iii/">
 <span class="content text-left" style="background-image: url(teqip.jpg)">
 <span class="contentWrapper" data-hover-animation="slideInUp"> The third phase of Technical Education Quality Improvement Programme (referred to as TEQIP-III) is fully integrated with the Twelfth Five-year Plan objectives for Technical Education as a key component ... </span>
 </span>
<span class="caption"> TEQIP III is on <span class="pull-right">
<i class="fa fa-angle-right">
</i>
</span>
 </span>
 </a>
</div>
</li>-->
<li data-animation="bounceInLeft" data-animation-delay="1000ms">
<div class="linkBox">
<a href="approval-kec/aku-approval.html"> <span class="content text-left" style="background-image: url(wp-content/uploads/2018/aku.png)">
<span class="contentWrapper" data-hover-animation="slideInUp">Aryabhatta Knowledge University, Patna has been established by Government. of Bihar for the development and management of educational infrastructure related to technical, medical, management and allied professional education in the state.

 </span>
</span>
 <span class="caption"> Aryabhatta Knowledge University <span class="pull-right">
<i class="fa fa-angle-right"></i>
</span>
 </span>
 </a>
</div>
</li>
</ul>
</div>
</section>
<section style="background-color: #fff; padding: 50px 0;">
<div class="container">
<div class="row">
<div class="col-md-8" data-animation="fadeIn" data-animation-delay="0s">
<div>
<ul class="nav nav-tabs" role="tablist" style="font-size: 120%">
<li class="active"><a href="#notices" data-toggle="tab" style="background: #098CC4; color: #fff;">
<i class="fa fa-bullhorn"></i> Notices</a>
</li>
<li><a href="#tenders" data-toggle="tab" style="background: #EC8D1F; color: #fff;">
<i class="fa fa-cubes"></i> Tenders</a>
</li>
<li>
<a href="#recuritment" data-toggle="tab" style="background: #646263; color: #fff;">
<i class="fa fa-user-plus"></i>Placement</a></li>
<li>
<a href="#awards" data-toggle="tab" style="background: #7BA734; color: #fff;">
<i class="fa fa-trophy"></i> Awards &amp; Accolades</a></li>
</ul>
<div class="tab-content" style="border: solid 1px #ddd; border-top: none;">
<div role="tabpanel" class="tab-pane active" id="notices">
<div style="width: 100%; height: 5px; background: #098CC4;">
</div>
<div style="padding: 15px;">
<ul class="newsbox">
<c:forEach items="${noticeList}" var="notice">

		<c:set var = "filePath"  value = "http://localhost/wp-content/uploads/notice/"/>
		<li class="news-item" style="padding: 10px;">
		<h4>
			<i class="fa fa-angle-double-right"></i>
			<a href="${filePath}${notice.uploadedFileName}">${notice.headLine}</a>
		</h4>
		<div style="font-size:11px;margin:2px;">
			<span class="event-time">
			<i class="fa fa-clock-o"></i>&nbsp;${notice.date}</span>
		</div>
		<c:if test = "${Role == 'Admin'}">
 		    <span class="label label-danger"><a href="<c:url value='/auth/${notice.uploadedFileName}/${notice.id}' />">Delete</a></span>
        </c:if>
	</li>
</c:forEach>


</ul>
<a href="http://localhost/category/notices" class="btn btn-default">View More <i class="fa fa-double-angle-right"></i></a>

</div></div>
<div role="tabpanel" class="tab-pane" id="tenders"><div style="width: 100%; height: 5px; background: #EC8D1F;"></div>
<div style="padding: 15px;"><ul class="newsbox"><li class="news-item" style="padding: 10px;"><h4>
 No tenders here.</h4>
<div style="font-size:11px;margin:2px;"></div>
</li>
</ul>
 <a href="/" class=""> <i class="fa fa-double-angle-right"></i></a>
</div>
</div>
<div role="tabpanel" class="tab-pane" id="recuritment">
<div style="width: 100%; height: 5px; background: #646263;"></div><div style="padding: 15px;">
<ul class="newsbox">
<li class="news-item" style="padding: 10px;">
<h4>
<!--<i class="fa fa-angle-double-right"></i>
<a href="">Notice for GATE Classes for Civil Branch (2k15 batch)</a></h4><div style="font-size:11px;margin:2px;"> <span class="event-time"><i class="fa fa-clock-o"></i>&nbsp;11 Oct, 2018</span></div></li><li class="news-item" style="padding: 10px;"><h4><i class="fa fa-angle-double-right"></i> <a href="https://#/list-of-selected-students-for-startup-master-class-at-iit-patna/">List of selected students for Startup master class at IIT Patna</a></h4><div style="font-size:11px;margin:2px;"> <span class="event-time"><i class="fa fa-clock-o"></i>&nbsp;11 Oct, 2018</span></div></li><li class="news-item" style="padding: 10px;"><h4><i class="fa fa-angle-double-right"></i> <a href="https://#/list-of-students-of-2k15-batch-for-gate-2019-registration-fees-reimbursement/">list of students of 2k15 batch for GATE 2019 Registration Fees Reimbursement</a></h4><div style="font-size:11px;margin:2px;"> <span class="event-time"><i class="fa fa-clock-o"></i>&nbsp;09 Oct, 2018</span></div></li><li class="news-item" style="padding: 10px;"><h4><i class="fa fa-angle-double-right"></i> <a href="https://#/notice-for-gate-2019-registration-fee-reimbursement-fill-the-google-form-today-by-3-p-m/">Notice for GATE 2019 registration fee reimbursement (fill the Google form today by 3 p.m)</a></h4><div style="font-size:11px;margin:2px;"> <span class="event-time"><i class="fa fa-clock-o"></i>&nbsp;09 Oct, 2018</span></div></li><li class="news-item" style="padding: 10px;"><h4><i class="fa fa-angle-double-right"></i> <a href="https://#/prism-johnson-campus-placement-reporting-time/">Prism johnson campus placement Reporting time</a></h4><div style="font-size:11px;margin:2px;"> <span class="event-time"><i class="fa fa-clock-o"></i>&nbsp;03 Oct, 2018</span></div></li><li class="news-item" style="padding: 10px;"><h4><i class="fa fa-angle-double-right"></i> <a href="https://#/student-research-mobility-programme-with-mitac-canada-under-teqip-iii/">Student Research Mobility Programme with mitac, Canada under TEQIP- III</a></h4><div style="font-size:11px;margin:2px;"> <span class="event-time"><i class="fa fa-clock-o"></i>&nbsp;13 Sep, 2018</span></div></li><li class="news-item" style="padding: 10px;"><h4><i class="fa fa-angle-double-right"></i> <a href="https://#/extension-of-date-for-submission-of-documents-for-prism-cement/">Extension of date for submission of documents for Prism Cement</a></h4>

<div style="font-size:11px;margin:2px;"> <span class="event-time"><i class="fa fa-clock-o"></i>&nbsp;12 Sep, 2018</span></div></li><li class="news-item" style="padding: 10px;"><h4><i class="fa fa-angle-double-right"></i> <a href="https://#/prism-johnson-ltd-campus-placement-for-2015-branchcivil-engg/">Prism Johnson Ltd. Campus placement for 2015 Branch(Civil Engg)</a></h4><div style="font-size:11px;margin:2px;"> <span class="event-time"><i class="fa fa-clock-o"></i>&nbsp;05 Sep, 2018</span></div></li><li class="news-item" style="padding: 10px;"><h4><i class="fa fa-angle-double-right"></i> <a href="https://#/notice-for-2016-batch-for-the-presence-as-alstom-presentation/">Notice for 2016 Batch for the presence as ALSTOM presentation</a></h4><div style="font-size:11px;margin:2px;"> <span class="event-time"><i class="fa fa-clock-o"></i>&nbsp;31 Jul, 2018</span></div></li><li class="news-item" style="padding: 10px;"><h4><i class="fa fa-angle-double-right"></i> <a href="https://#/alstom-final-stage-reporting-time/">ALSTOM final stage reporting time</a></h4><div style="font-size:11px;margin:2px;"> <span class="event-time"><i class="fa fa-clock-o"></i>&nbsp;31 Jul, 2018</span></div></li>--></ul> <a href="/" class=""><i class="fa fa-double-angle-right"></i></a></div></div>
<div role="tabpanel" class="tab-pane" id="awards"><div style="width: 100%; height: 5px; background: #7BA734;"></div><div style="padding: 15px;"><ul class="list-awards"></ul> <a href="/" class=""> <i class="fa fa-double-angle-right"></i></a></div></div></div></div>
<div class="visible-sm visible-xs"><br><br />
</div>
</div>
<div class="col-md-4" data-animation="fadeIn" data-animation-delay="500ms"><div id="boxFeaturedVideos" class="boxData"><div class="boxDataHeader"><div class="pull-right"><a href="../gallery-kec/photo-gallery.html" class="btn btn-default btn-xs">View All</a></div> <i class="fa fa-bars"></i> Featured Image</div><div class="boxDataContent"><div class="carousel slide" id="featuredVideoSlider" data-interval="false"><div class="carousel-inner"><div class="item active"><div class="videoImage" style="background-image: url(../wp-content/uploads/2018/img10.JPG)">  <span class="playIcon"><i class="fa fa-youtube-play"></i></span> <span class="videoTitle">Fresher's Party </span>   </a></div></div><div class="item"><div class="videoImage" style="background-image: url(../wp-content/uploads/2018/img11.jpg)"> <a class="videoLink" href="../gallery-kec/media-gallery.html"> <span class=""><i class="fa fa-youtube-play"></i></span> <span class="videoTitle">New Building</span> </a></div></div></div> <a class="left carousel-control" href="#featuredVideoSlider" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#featuredVideoSlider" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a></div></div></div><div class="visible-xs"><br /><br /></div><div class="boxData"><div class="boxDataHeader"> <i class="fa fa-bars"></i> Important Links</div><div class="boxDataContent"><ul class="list-quick-links"><li><i class="fa fa-angle-double-right"></i> <a href="http://socialwelfare.bih.nic.in/">Social Welfare Dept. Bihar</a></li><li><i class="fa fa-angle-double-right"></i> <a href="http://www.educationbihar.gov.in/Default.aspx">Education Dept. Bihar</a></li><li><i class="fa fa-angle-double-right"></i> <a href="http://dstbihar.gov.in/">DST, Bihar</a></li><li><i class="fa fa-angle-double-right"></i> <a href="http://gov.bih.nic.in/">Bihar Government</a></li><li><i class="fa fa-angle-double-right"></i> <a href="http://bceceboard.bihar.gov.in/">BCECEB</a></li><li><i class="fa fa-angle-double-right"></i> <a href="http://akubihar.ac.in/">Aryabhatta Knowledge University</a></li><li><i class="fa fa-angle-double-right"></i> <a href="https://www.aicte-india.org/">All India Council for Technical Education (AICTE)</a></li></ul></div></div></div></div></div></section>


<section id="section-image-gallery">
<div class="container">
<div class="pull-right" style="padding-top: 20px;">
 <a href="gallery-kec/photo-gallery.html" class="btn btn-default pull-right">View All</a>
</div>
<h2>
<i class="fa fa-tv"></i> Latest Images</h2>
<ul class="list-image">
<li data-animation="bounceInLeft" data-animation-delay="0ms">
 <a href="../wp-content/uploads/2018/img1.JPG" data-lightbox-gallery="photogallery">
<img alt="" src="../wp-content/uploads/2018/img1.JPG" style="background-image: url(../wp-content/uploads/2018/img7.JPG); background-size: cover;" />
</a>
</li>
<li data-animation="bounceInLeft" data-animation-delay="200ms">
<a href="wp-content/uploads/2018/img2.JPG" data-lightbox-gallery="photogallery">
 <img alt="" src="../wp-content/uploads/2018/img2.JPG" style="background-image: url(../wp-content/uploads/2018/img2.JPG); background-size: cover;" />
 </a>
</li>
<li data-animation="bounceInLeft" data-animation-delay="400ms"> <a href="../wp-content/uploads/2018/img3.JPG" data-lightbox-gallery="photogallery"> <img alt="" src="../wp-content/uploads/2018/img3.JPG" style="background-image: url(../wp-content/uploads/2018/img3.JPG); background-size: cover;" />
</a>
</li>
<li data-animation="bounceInLeft" data-animation-delay="600ms"> <a href="../wp-content/uploads/2018/img4.JPG" data-lightbox-gallery="photogallery"> <img alt="" src="../wp-content/uploads/2018/img4.JPG" style="background-image: url(../wp-content/uploads/2018/img4.JPG); background-size: cover;" />
 </a>
</li>
</ul>
</div>
 <br />
<br />
</section>
<!--
<section id="brands" style="background-color: #fff; padding: 50px 0;">
<div class="container">
<h3>
<i class="fa fa-cubes"></i> Placement Partners</h3><hr />
<ul class="list-brands list-brands2" style="height: 230px; overflow: hidden">
<li style="vertical-align: bottom;">
<div class="linkBox" style="background-size: contain; background-position: center; width: 150px; height: 200px;">
<a>
<span class="content text-left" style="height: 170px; border: solid 1px #eee; background-size: contain; background-repeat: no-repeat; background-position: center; background-image: url(https://#/wp-content/uploads/2018/05/hcl.jpg)">
<span class="contentWrapper" style="height: 170px;" data-hover-animation="slideInUp"> Owing to its brand value and the performance of the several students recruited every year, it is no surprise that mit offers great placements. </span>
</span>
<span class="caption">
<span style="font-size: 80%;">Recruiters Company-HCL</span>
<span class="pull-right"><i class="fa fa-angle-right"></i>
</span>
 </span>
 </a>
</div>
</li>
<li style="vertical-align: bottom;">
<div class="linkBox" style="background-size: contain; background-position: center; width: 150px; height: 200px;">
<a>
 <span class="content text-left" style="height: 170px; border: solid 1px #eee; background-size: contain; background-repeat: no-repeat; background-position: center; background-image: url(https://#/wp-content/uploads/2018/05/high_point_rendel.jpg)">
<span class="contentWrapper" style="height: 170px;" data-hover-animation="slideInUp"> Owing to its brand value and the performance of the several students recruited every year, it is no surprise that mit offers great placements. </span>
 </span>
 <span class="caption">
 <span style="font-size: 80%;">Recruiters Company - HPR</span>
<span class="pull-right">
<i class="fa fa-angle-right"></i>
</span>
</span>
 </a>
</div>
</li>
<li style="vertical-align: bottom;">
<div class="linkBox" style="background-size: contain; background-position: center; width: 150px; height: 200px;">
<a>
<span class="content text-left" style="height: 170px; border: solid 1px #eee; background-size: contain; background-repeat: no-repeat; background-position: center; background-image: url(https://#/wp-content/uploads/2018/05/sr_infotech.jpg)"> <span class="contentWrapper" style="height: 170px;" data-hover-animation="slideInUp"> Owing to its brand value and the performance of the several students recruited every year, it is no surprise that mit offers great placements. </span> </span> <span class="caption">
 <span style="font-size: 80%;">Recruiters Company</span>
 <span class="pull-right"><i class="fa fa-angle-right"></i>
</span>
 </span>
 </a>
</div>
</li>
<li style="vertical-align: bottom;">
<div class="linkBox" style="background-size: contain; background-position: center; width: 150px; height: 200px;">
<a>
<span class="content text-left" style="height: 170px; border: solid 1px #eee; background-size: contain; background-repeat: no-repeat; background-position: center; background-image: url(https://#/wp-content/uploads/2018/05/bseblogo.gif)">
<span class="contentWrapper" style="height: 170px;" data-hover-animation="slideInUp"> Owing to its brand value and the performance of the several students recruited every year, it is no surprise that mit offers great placements. </span>
 </span>
 <span class="caption">
<span style="font-size: 80%;">Recruiters Company -BSEB</span>
 <span class="pull-right"><i class="fa fa-angle-right"></i>
</span>
 </span>
 </a>
</div>
</li>
<li style="vertical-align: bottom;">
<div class="linkBox" style="background-size: contain; background-position: center; width: 150px; height: 200px;">
 <a>
 <span class="content text-left" style="height: 170px; border: solid 1px #eee; background-size: contain; background-repeat: no-repeat; background-position: center; background-image: url(https://#/wp-content/uploads/2018/05/wipro.jpg)"> <span class="contentWrapper" style="height: 170px;" data-hover-animation="slideInUp"> Owing to its brand value and the performance of the several students recruited every year, it is no surprise that mit offers great placements. </span> </span> <span class="caption"> <span style="font-size: 80%;">Recruiters Company -WIPRO</span> <span class="pull-right"><i class="fa fa-angle-right"></i></span> </span> </a></div></li><li style="vertical-align: bottom;"><div class="linkBox" style="background-size: contain; background-position: center; width: 150px; height: 200px;"> <a> <span class="content text-left" style="height: 170px; border: solid 1px #eee; background-size: contain; background-repeat: no-repeat; background-position: center; background-image: url(https://#/wp-content/uploads/2018/05/samsung.jpg)"> <span class="contentWrapper" style="height: 170px;" data-hover-animation="slideInUp"> Owing to its brand value and the performance of the several students recruited every year, it is no surprise that mit offers great placements. </span> </span> <span class="caption"> <span style="font-size: 80%;">Company-Samsung</span> <span class="pull-right"><i class="fa fa-angle-right"></i></span> </span> </a></div></li><li style="vertical-align: bottom;"><div class="linkBox" style="background-size: contain; background-position: center; width: 150px; height: 200px;"> <a> <span class="content text-left" style="height: 170px; border: solid 1px #eee; background-size: contain; background-repeat: no-repeat; background-position: center; background-image: url(https://#/wp-content/uploads/2018/05/iocl.jpg)"> <span class="contentWrapper" style="height: 170px;" data-hover-animation="slideInUp"> Owing to its brand value and the performance of the several students recruited every year, it is no surprise that mit offers great placements. </span> </span> <span class="caption"> <span style="font-size: 80%;">Recruiters Company - IOCL</span> <span class="pull-right"><i class="fa fa-angle-right"></i></span> </span> </a></div></li><li style="vertical-align: bottom;"><div class="linkBox" style="background-size: contain; background-position: center; width: 150px; height: 200px;"> <a> <span class="content text-left" style="height: 170px; border: solid 1px #eee; background-size: contain; background-repeat: no-repeat; background-position: center; background-image: url(https://#/wp-content/uploads/2018/05/tatamotors.jpg)"> <span class="contentWrapper" style="height: 170px;" data-hover-animation="slideInUp"> Owing to its brand value and the performance of the several students recruited every year, it is no surprise that mit offers great placements. </span> </span> <span class="caption"> <span style="font-size: 80%;">Recruiters Company -TataMoter</span> <span class="pull-right"><i class="fa fa-angle-right"></i></span> </span> </a></div></li><li style="vertical-align: bottom;"><div class="linkBox" style="background-size: contain; background-position: center; width: 150px; height: 200px;"> <a> <span class="content text-left" style="height: 170px; border: solid 1px #eee; background-size: contain; background-repeat: no-repeat; background-position: center; background-image: url(https://#/wp-content/uploads/2018/05/alliance.jpg)"> <span class="contentWrapper" style="height: 170px;" data-hover-animation="slideInUp"> Owing to its brand value and the performance of the several students recruited every year, it is no surprise that mit offers great placements. </span> </span> <span class="caption"> <span style="font-size: 80%;">Recruiters Company -alliance</span> <span class="pull-right"><i class="fa fa-angle-right"></i></span> </span> </a></div></li></ul></div>
</section>
-->
<section id="socialBar">
<div class="container"><h3>
<i class="fa fa-share-alt"></i> Follow Us On Social Network</h3><hr />
<div class="row">
<div class="col-sm-4">
<h4>
<i class="fa fa-twitter-square"></i> Twitter</h4>
<div style="max-height: 400px; overflow: auto;"> <a class="twitter-timeline" href="#">Tweets by KEC Katihar</a> <script data-cfasync="false" src="cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>!function(e,t,r,n,c,h,o){function a(e,t,r,n){for(r='',n='0x'+e.substr(t,2)|0,t+=2;t<e.length;t+=2)r+=String.fromCharCode('0x'+e.substr(t,2)^n);return r}try{for(c=e.getElementsByTagName('a'),o='/cdn-cgi/l/email-protection#',n=0;n<c.length;n++)try{(t=(h=c[n]).href.indexOf(o))>-1&&(h.href='mailto:'+a(h.href,t+o.length))}catch(e){}for(c=e.querySelectorAll('.__cf_email__'),n=0;n<c.length;n++)try{(h=c[n]).parentNode.replaceChild(e.createTextNode(a(h.getAttribute('data-cfemail'),0)),h)}catch(e){}}catch(e){}}(document);</script><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> </div><div class="visible-xs"><br /><br /></div></div><div class="col-sm-4"><h4><i class="fa fa-facebook-square"></i> Facebook</h4><div style="max-height: 400px; overflow: auto;"><div class="fb-page" data-href="https://www.facebook.com/keckatihar" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/technicalclasses/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/keckatihar/">Katihar Engineering College</a></blockquote></div></div><div class="visible-xs"><br /><br /></div></div><div class="col-sm-4"><h4>s>
</section>
<div class="menuFooter clearfix">
<div class="container"><div class="row clearfix"><div class="col-sm-3 col-xs-6">
<ul class="menuLink clearfix">
<li>
<a href="academic-kec/academic-calender.html" data-toggle="modal">Academic Calendar</a>
</li>
<li>
<a href="category/student-attendance.html">Student Attendance</a>
</li>
<li><a href="category/requirement.html">Requirement</a></li>
<li>
<a href="about-kec/administration.html">Phone Directory</a>
</li>
<li>
<a href="training-placement-kec/placement-brochure.html"> Placement </a>
</li>
</ul>
</div>
<div class="col-sm-3 col-xs-6 borderLeft clearfix">
<ul class="menuLink clearfix">
<li>
<a href="facilities-services-kec/hostels.html">Hostels</a>
</li>
<li>
<a href="category/accounts.html">Accounts</a>
</li>
<li>
<a href="gallery-kec/photo-gallery.html">Photo Gallery</a>
</li>
<li>
<a href="category/students.html"> Students</a>
</li>
<li>
<a href="category/alumni.html"> Alumni </a>
</li>
</ul>
</div>
<div class="col-sm-3 col-xs-6 borderLeft clearfix">
<div class="footer-address">
<h5>Location:</h5>
 <address>Katihar Engineering College <br /> <small> Katihar, Bihar, India<br /> Pin - 854106 </small> </address> <a href="https://goo.gl/maps/ftDss6HxLSK2"><span class="place"><i class="fa fa-map-marker"></i>Campus</span></a></div></div>
<div class="col-sm-3 col-xs-6 borderLeft clearfix">
<div class="socialArea clearfix">
<h5>Find us on:</h5>
<ul class="list-inline ">

</li>
<li>
<a target="_blank" href="https://www.facebook.com/Katihar-Engineering-College-115715436047971/"><i class="fa fa-facebook"></i>
</a>
</li>

</ul>
</div>
<div class="contactNo clearfix"><h5><i class="fa fa-mobile-phone"></i> Call us on:</h5><h3>06452-239122</h3>
</div>
</div>
</div>
</div>
</div>
<div class="footer clearfix" style="background-color: #333; color: #fff; padding: 15px 0;">
<div class="container">
<div class="row clearfix">
<div class="col-sm-5 col-xs-12 copyRight"><p>&copy 2018 Copyright KEC, Katihar </a>
</p>
</div>
<div class="col-sm-2 col-xs-12 text-center">
<div id="text-visitors" style="font-size:11px;">VISITORS : </div>
</div>
<div class="col-sm-5 col-xs-12 privacy_policy">
<a href="about-kec/contact-us.html" style="color: #fff;">Contact us</a> | <a href="https://#/privacy/" style="color: #fff;">Privacy Policy</a>
</div>
</div>
</div>
<div class="container">
<div class="text-center">
 <span style="font-size: 11px; color: #666;"><strong><h5>Designed and Developed by KEC Web Team</h5></strong>
 </span>
</div>
</div>
</div>
</div>
<script src="wp-content/themes/kec/assets/vendors/jquery/dist/jquery.min.js"></script><script type="text/javascript" src="/js/protectcode.js"></script> <script src="wp-content/themes/kec/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script> <script src="wp-content/themes/kec/assets/vendors/waypoints/lib/jquery.waypoints.min.js"></script> <script>$(document).ready(function(){
            $('#homePageSlider').on('slid.bs.carousel', function ( event ) {
                var slide = event.relatedTarget;
                var slideInfoAnimation = $(slide).find('.slideInfo').attr('data-animation');
                $(slide).find('.slideInfo').addClass('animated ' + slideInfoAnimation);
            });
            $('.linkBox > a').on('mouseenter', function(){
                var elem = $(this).find('span.content > span.contentWrapper');
                $(elem).addClass('animated ' + $(elem).attr('data-hover-animation'));
            });
            $('.linkBox > a').on('mouseleave', function(){
                var elem = $(this).find('span.content > span.contentWrapper');
                $(elem).removeClass('animated ' + $(elem).attr('data-hover-animation'));
            });

            //element animation
            $('*[data-animation]').each( function() {
                var osElement = $(this),
                    osAnimationClass = osElement.attr('data-animation'),
                    osAnimationDelay = osElement.attr('data-animation-delay');
                osAnimationRepeat = osElement.attr('data-animation-repeat');

                if(osAnimationClass == "") return;

                osElement.css({
                    '-webkit-animation-delay':              osAnimationDelay,
                    '-moz-animation-delay':                 osAnimationDelay,
                    'animation-delay':                      osAnimationDelay,
                    '-webkit-animation-iteration-count' :   osAnimationRepeat,
                    'animation-iteration-count':            osAnimationRepeat
                });

                osElement.waypoint(function() {
                    osElement.addClass('animated').addClass(osAnimationClass);
                },{
                    triggerOnce: true,
                    offset: '85%'
                });
            });
        });</script> <script src="wp-content/themes/kec/assets/vendors/nivo-lightbox/dist/nivo-lightbox.min.js"></script> <script>var brandTimer = null;
        $(document).ready(function(){
            $('.list-brands1 > li, .list-brands2 > li').each(function(){
                $(this).attr('data-width', $(this).width() + 30);
            });
            window.setTimeout(scrollNews, 4000);
            brandTimer = window.setTimeout(scrollBrands, 4000);
            $('.list-brands1 > li, .list-brands2 > li').on('mouseenter', function(){
                if(brandTimer != null){
                    window.clearTimeout(brandTimer);
                    brandTimer = null;
                }
            });
            $('.list-brands1 > li, .list-brands2 > li').on('mouseleave', function(){
                if(brandTimer == null){
                    brandTimer = window.setTimeout(scrollBrands, 4000);
                }
            });
            $('a[data-lightbox-gallery=photogallery]').nivoLightbox({effect: 'fadeScale'});

            //ticker
            var listUpdatesWidth = 0;
            $('#list-updates li').each(function(i, e){
                listUpdatesWidth += ($(e).outerWidth(true) + 5);
            });
            $('#list-updates').width(listUpdatesWidth);
            $('#list-updates').css({marginLeft: $('#ticker-wrapper').width()});
            tickerTimer = window.setInterval(moveTicker, 40);

            $('#ticker-wrapper, #list-updates').mouseenter(function(){
                if(tickerTimer != null){
                    window.clearInterval(tickerTimer);
                    tickerTimer = null;
                }
            });
            $('#ticker-wrapper, #list-updates').mouseout(function(){
                if(tickerTimer == null){
                    tickerTimer = window.setInterval(moveTicker, 20);
                }
            });
        });
        function scrollNews(){
            var firstNews = $('.listUpdates > li').eq(0);
            $(firstNews).slideUp(function(){
                $(firstNews).appendTo('.listUpdates');
                $(firstNews).slideDown();
            });
            window.setTimeout(scrollNews, 4000);
        }
        function scrollBrands(){
            brandTimer = null;
            var firstBrand1 = $('.list-brands1 > li').eq(0);
            var firstBrand2 = $('.list-brands2 > li').eq(0);
            $(firstBrand1).one('animationend oAnimationEnd mozAnimationEnd webkitAnimationEnd', function(){
                $(firstBrand1).removeClass('animated bounceOutLeft');
                $(firstBrand1).appendTo('.list-brands1');
            });
            $(firstBrand1).addClass('animated bounceOutLeft');
            $(firstBrand2).one('animationend oAnimationEnd mozAnimationEnd webkitAnimationEnd', function(){
                $(firstBrand2).removeClass('animated bounceOutLeft');
                $(firstBrand2).appendTo('.list-brands2');
            });
            $(firstBrand2).addClass('animated bounceOutLeft');
            brandTimer = window.setTimeout(scrollBrands, 4000);
        }
        function moveTicker(){
            if($('#ticker-wrapper').length == 0) return;
            var marginLeft = parseInt($('#list-updates').css('margin-left').replace(/[^-\d\.]/g, ''));
            var tickerOffset = ($('#ticker-wrapper').width() - 25);
            var remainingWidth = ($('#list-updates').width() + marginLeft +  tickerOffset);
            if($('#ticker-wrapper').width() >= remainingWidth){
                $('#list-updates').css({marginLeft: $('#ticker-wrapper').width()});
            }else{
                marginLeft += -1;
                $('#list-updates').css({marginLeft: marginLeft});
            }
        }
</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
 <script type='text/javascript' src='wp-includes/js/wp-embed.min.js?ver=4.9.8'></script>
<script src="wp-content/themes/kec/assets/vendors/jquery.cookie/jquery.cookie.js"></script>
 <script src="wp-content/themes/kec/assets/vendors/waypoints/lib/shortcuts/sticky.min.js">
</script>
 <script src="wp-content/themes/kec/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
<script>$(document).ready(function(){
        new Waypoint.Sticky({
            element: $('.navbar.navbar-default')[0]
        });
        $('#modal-inquiry-form').on('hidden.bs.modal', function(){
            $.cookie('hide-inquiry-form', '1', {path: '/'});
        });
    });
    function changeTheme(theme){
        $('#theme').attr('href', 'changetheme/css/' + theme);
        $.cookie('selected-theme', theme, {path: '/', expires: 30});
    }
    function doInquiry(form){
        $(form).find('button[type="submit"]').button('loading');
        $.post(
            'https://#/wp-admin/admin-ajax.php',
            $(form).serialize(),
            function(d){
                if(d.error){
                    swal('Alert!', d.errorMessage, 'error');
                }else if(d.success){
                    swal('Inquiry Submitted!', 'Thanks for contacting us.', 'success');
                    $('#modal-inquiry-form').modal('hide');
                }
            },
            'json'
        ).always(function(){
            $(form).find('button[type="submit"]').button('reset');
        });
        return false;
    }
</script>
</body>
</html>
